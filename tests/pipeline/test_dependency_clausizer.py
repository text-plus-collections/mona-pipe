# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

from spacy.tokens import Span, Token

import monapipe.model
import monapipe.pipeline.clausizer.dependency_clausizer
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_dependency_clausizer():
    """Integration test for the `dependency_clausizer`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("dependency_clausizer")
    doc = nlp(text_goethe_wv)
    assert check_data_types([doc], "clauses", list, Span)
    assert check_data_types(doc.sents, "clauses", list, Span)
    assert check_data_types(doc._.clauses, "tokens", list, Token)
    assert check_data_types(doc._.clauses, "prec_punct", list, Token)
    assert check_data_types(doc._.clauses, "succ_punct", list, Token)
    assert check_data_types(doc, "clause", Span)
