# Components and implementations

For detailed information about spaCy and its components, please follow the [documentation of spaCy](https://spacy.io/usage/spacy-101).

The elements of a spaCy pipeline are *components*. Some components are already included in spaCy ("built-in"). Additionally, users can add own ["custom" components](https://spacy.io/usage/processing-pipelines#custom-components), as it is done for MONAPipe. The pretrained model already contains the built-in components [Tokenizer](https://spacy.io/usage/linguistic-features#tokenization), [Tok2Vec](https://spacy.io/usage/linguistic-features#vectors-similarity), [Tagger](https://spacy.io/usage/linguistic-features#pos-tagging), [Morphologizer](https://spacy.io/usage/linguistic-features#morphology), [Sentencizer](https://spacy.io/usage/linguistic-features#sbd), [Lemmatizer](https://spacy.io/usage/linguistic-features#lemmatization), [DependencyParser](https://spacy.io/usage/linguistic-features#dependency-parse) and [EntityRecognizer](https://spacy.io/usage/linguistic-features#named-entities). Other built-in components are predifened by spaCy and a user can load and/or train them additionally, like the [EntityLinker](https://spacy.io/api/entitylinker).

![Overview of MONAPipe's components and implementations](monapipe.drawio.svg)

We use the term *implementation* for a specific realisation of a component. For example, the custom component [SpeechTagger](?ref_type=heads#speechtagger) has two different implementations: [`flair_speech_tagger`](?ref_type=heads#flair_speech_tagger) and [`quotation_marks_speech_tagger`](?ref_type=heads#quotation_marks_speech_tagger). You can find an overview of all built-in and custom components/implementations in the [Table of components and implementations](#table-of-components-and-implementations) below.

MONAPipe comes with a non-default languge model for German that was trained on German Universal Dependencies treebanks (PUD, LIT, GSD; cf. [Dönicke (2023)](references.md)). Therefore, for all components except of the `EntityRecognizer`, the MONAPipe language model replaces the built-in implementations by its own custom implementations (see table in next paragraph).


## Table of components and implementations


The table below shows built-in and custom components and their available implementations. Only components and implementations within MONAPipe and the MONAPipe language model (not the language models from spaCy) are described in the table.
For each implementation you find requirements (other components to load before), the object type with spaCy attribute and its return type, and a short description.

Some external implementations that can be used in combination with MONAPipe are also listed.

<table>
    <thead>
        <tr>
            <th>Component</th>
            <th>Implementation</th>
            <th>Requires</th>
            <th>Object.Attribute -> Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="5" style="text-align: center"><b>Built-in components</b><br/><i>s: spaCy / m: monapipe / e: external; o: online API / c: local container API </i></td>
        </tr>
        <tr>
            <td rowspan="2"><a href="https://spacy.io/usage/linguistic-features#dependency-parse">DependencyParser</a></td>            
            <td rowspan="2"><tt>parser</tt> (m)</td>
            <td rowspan="2">Sentencizer</td>
            <td><tt>token.dep_</tt> -> <tt>str</tt></td>
            <td>Dependency label.</td>
        </tr>
        <tr>
            <td><tt>token.head</tt> -> <tt>Token</tt></td>
            <td>Dependency head.</td>
        </tr>
        <tr>
            <td rowspan="4">
                <a href="https://spacy.io/usage/linguistic-features#entity-linking">EntityLinker
                </a>
            </td>
            <td rowspan="4">
                <a href="?ref_type=head#spacy-dbpedia-spotlight"><tt>dbpedia_spotlight</tt></a> (e, o)<br/><br/>
                <!-- <a href="?ref_type=head#spacyfishing"><tt>entityfishing</tt></a> (e, o)<br/><br/>
                <a href="?ref_type=head#spacyopentapioca"><tt>opentapioca</tt></a> (e, o) -->
            </td>
            <td rowspan="4">EntityRecognizer</td>
            <td><tt>span.kb_id_</tt> -> <tt>str</tt></td>
            <td>
                dbpedia-spotlight:<br/> URI of DBpedia.<br/><br/>
                entityfishing:<br/> Per default empty.<br/><br/>
                opentapioca:<br/> Wikidata ID.
            </td>
        </tr>
        <tr>
            <td><tt>span.label_</tt> -> <tt>str</tt></td>
            <td>
                dbpedia-spotlight:<br/>Always 'DBPEDIA_ENT'.<br/><br/>
                entityfishing:<br/> Entity label, e.g. `PER`, `LOC`.<br/><br/>
                opentapioca:<br/>Wikidata label, e.g. 'PERSON' or 'LOC'.
            </td>   
        </tr>
        <tr>
            <td colspan="2"><i>custom attributes for dbpedia-spotlight:</i></td>
        </tr>
        <tr>
            <td><tt>span._.dbpedia_raw_result</tt> -> <tt>Dict[str]</tt></td>
            <td>Raw json for the entity from DBpedia spotlight as Python Dict, keys: `@URI`, `@support`, `@types`, `@surfaceForm`, `@offset`, `@similarityScore``, `@percentageOfSecondRank`.</td>
        </tr>
        <!-- <tr>
            <td colspan="2"><i>custom attributes for entityfishing:</i></td>
        </tr>
        <tr>
            <td><tt>span._.kb_qid</tt> -> <tt>str</tt></td>
            <td>Wikidata ID.</td>
        </tr>
        <tr>
            <td><tt>span._.url_wikidata</tt> -> <tt>str</tt></td>
            <td>URL of wikidata entry.</td>
        </tr>
        <tr>
            <td><tt>span._.wikipedia_page_ref</tt> -> <tt>str</tt></td>
            <td>Identifier of the Wikipedia concept.</td>
        </tr>
        <tr>
            <td><tt>span._.nerd_score</tt> -> <tt>float</tt></td>
            <td>Selection confidence score for the disambiguated entity.</td>
        </tr>
        <tr>
            <td><tt>span._.normal_term</tt> -> <tt>str</tt></td>
            <td>The normalised term name.</td>
        </tr>
        <tr>
            <td><tt>span._.description</tt> -> <tt>str</tt></td>
            <td>Short concept definition from Wikipedia with wikicode.</td>
        </tr>
        <tr>
            <td><tt>span._.src_description</tt> -> <tt>str</tt></td>
            <td>The name of the Wikipedia KB from which the definition comes from (eg. wikipedia-en).</td>
        </tr>
        <tr>
            <td><tt>span._.other_ids</tt> -> <tt>List[dict]</tt></td>
            <td>Other statements in the KB related to a Wikipedia concept.</td>
        </tr>
        <tr>
            <td colspan="2"><i>custom attributes for opentapioca:</i></td>
        </tr>
        <tr>
            <td><tt>span._.description</tt> -> <tt>List[str]</tt></td>
            <td>Description of the linked entity from Wikidata.</td>
        </tr>
        <tr>
            <td><tt>span._.score</tt> -> <tt>float</tt></td>
            <td>Linking score.</td>
        </tr>
        <tr>
            <td><tt>span._.types</tt> -> <tt>Dict[str, bool]</tt></td>
            <td>Dict of relations to types.</td>
        </tr>
        <tr>
            <td><tt>span._.aliases</tt> -> <tt>List[str]</tt></td>
            <td>List of aliases.</td>
        </tr> -->
        <tr>
            <td rowspan="3"><a href="https://spacy.io/usage/linguistic-features#named-entities">EntityRecognizer</a></td>
            <td rowspan="3"><tt>ner</tt> (s)<br/><br/><a href="?ref_type=heads#bert_character_ner"><tt>bert_character_ner</tt></a> (m)</td>
            <td rowspan="3">-</td>
            <td><tt>doc.ents</tt> -> <tt>Iterable[Span]</tt>, <tt>span.ents</tt> -> <tt>Iterable[Span]</tt></td>
            <td>List of named entities.</td>
        </tr>
        <tr>
            <td><tt>token.ent_iob_</tt> -> <tt>str</tt></td>
            <td>Namend entity B-I-O tag.</td>
        </tr>
        <tr>
            <td><tt>token.ent_type_</tt> -> <tt>str</tt></td>
            <td>Namend entity type.</td>
        </tr>
        <tr>
            <td><a href="https://spacy.io/usage/linguistic-features#lemmatization">Lemmatizer</a></td>
            <td><tt>trainable_lemmatizer</tt> (m)</td>
            <td>-</td>
            <td><tt>token.lemma</tt> -> <tt>str</tt></td>
            <td>Lemma.</td>
        </tr>
        <tr>
            <td><a href="https://spacy.io/usage/linguistic-features#morphology">Morphologizer</a></td>
            <td><tt>morphologizer</tt> (m)</td>
            <td>-</td>
            <td><tt>token.morph</tt> -> <tt>MorphAnalysis</tt></td>
            <td>Morphological features.</td>
        </tr>
        <tr>
            <td rowspan="3"><a href="https://spacy.io/usage/linguistic-features#sbd">Sentencizer</a></td>
            <td rowspan="3"><tt>sentencizer</tt> (m) *</td>
            <td rowspan="3">-</td>
            <td><tt>doc.sents</tt> -> <tt>Iterable[Span]</tt></td>
            <td>List of sentences.</td>
        </tr>
        <tr>
            <td><tt>token.is_sent_start</tt> -> <tt>boolean</tt></td>
            <td>First token of a sentence?</td>
        </tr>
        <tr>
            <td><tt>token.sent</tt> -> <tt>Span</tt></td>
            <td>Sentence.</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="https://spacy.io/usage/linguistic-features#pos-tagging">Tagger</a></td>
            <td rowspan="2"><tt>tagger</tt> (m)</td>
            <td rowspan="2">-</td>
            <td><tt>token.pos_</tt> -> <tt>str</tt></td>
            <td>UPOS (coarse-grained POS) tag.</td>
        </tr>
        <tr>
            <td><tt>token.tag_</tt> -> <tt>str</tt></td>
            <td>STTS (fine-grained POS) tag.</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="https://spacy.io/usage/linguistic-features#vectors-similarity">Toc2Vec</a></td>
            <td rowspan="2"><tt>toc2vec</tt> (m)</td>
            <td rowspan="2">-</td>
            <td><tt>token.vector</tt> -> <tt>numpy.ndarray</tt></td>
            <td>A 1-dimensional array representing the token's vector.</td>
        </tr>
        <tr>
            <td><tt>token.vector_norm</tt> -> <tt>numpy.float32</tt></td>
            <td>The L2 norm of the vector representation.</td>
        </tr>
        <tr>
            <td rowspan="8"><a href="https://spacy.io/usage/linguistic-features#tokenization">Tokenizer</a></td>
            <td rowspan="8"><tt>tokenizer</tt> (m) **<br/></td>
            <td rowspan="8">-</td>
            <td><tt>doc.text</tt> -> <tt>str</tt></td>
            <td>Document's text.</td>
        </tr>
        <tr>
            <td><tt>span.end</tt> -> <tt>int</tt></td>
            <td>Span's end index in document.</td>
        </tr>
        <tr>
            <td><tt>span.start</tt> -> <tt>int</tt></td>
            <td>Span's start index in document.</td>
        </tr>
        <tr>
            <td><tt>span.text</tt> -> <tt>str</tt></td>
            <td>Span's text.</td>
        </tr>
        <tr>
            <td><tt>token.i</tt> -> <tt>int</tt></td>
            <td>Token's index in document.</td>
        </tr>
        <tr>
            <td><tt>token.idx</tt> -> <tt>int</tt></td>
            <td>Token's character offset in document.</td>
        </tr>
        <tr>
            <td><tt>token.text</tt> -> <tt>str</tt></td>
            <td>Token's text.</td>
        </tr>
        <tr>
            <td><tt>token.whitespace_</tt> -> <tt>str</tt></td>
            <td>Token's trailing whitespace.</td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center"><b>Custom components</b><br/><i>m: monapipe / e: external; o: online API / c: local container API</i></td>
        </tr>
        <tr>
            <td rowspan="2"><a href="?ref_type=heads#annotationreader">AnnotationReader</a></td>
            <td rowspan="2"><a href="?ref_type=heads#catma_annotation_reader"><tt>catma_annotation_reader</tt></a> (m)</td>
            <td rowspan="2"></td>
            <td><tt>doc._.annotations</tt> -> <tt>AnnotationList</tt></td>
            <td>Annotations for the piped text.</td>
        </tr>
        <tr>
            <td><tt>token._.annotations</tt> -> <tt>AnnotationList</tt></td>
            <td>Annotations for a token.</td>
        </tr>
        <tr>
            <td><a href="?ref_type=heads#attributiontagger">AttributionTagger</a></td>
            <td><a href="?ref_type=heads#neural_attribution_tagger"><tt>neural_attribution_tagger</tt></a> (m)</td>
            <td>Clausizer</td>
            <td><tt>span._.attribution</tt> -> <tt>Set[str]</tt></td>
            <td>Set of speaker labels for a clause (Span).</td>
        </tr>
        <tr>
            <td rowspan="6"><a href="?ref_type=heads#clausizer">Clausizer</a></td>
            <td rowspan="6"><a href="?ref_type=heads#dependency_clausizer"><tt>dependency_clausizer</tt></a> (m)</td>
            <td rowspan="6">DependencyParser<br/>Morphologizer<br/>Lemmatizer<br/>Tok2Vec</td>
            <td><tt>doc._.clauses</tt> -> <tt>List[Span]</tt></td>
            <td>List of clauses (Span) from a Doc.</td>
        </tr>
        <tr>
            <td><tt>span._.clauses</tt> -> <tt>List[Span]</tt></td>
            <td>List of clauses (Span) from a sentence (Span).</td>
        </tr>
        <tr>
            <td><tt>span._.prec_punct</tt> -> <tt>List[Token]</tt> -> <tt>List[Token]</tt></td>
            <td>Preceding punctuation tokens of a clause (Span).</td>
        </tr>
        <tr>
            <td><tt>span._.succ_punct</tt> -> <tt>List[Token]</tt></td>
            <td>Succeding punctuation tokens of a clause (Span).</td>
        </tr>
        <tr>
            <td><tt>span._.tokens</tt> -> <tt>List[Token]</tt></td>
            <td>Tokens of a clause (Span).</td>
        </tr>
        <tr>
            <td><tt>token._.clause</tt> -> <tt>Span</tt></td>
            <td>Superordinate clause (Span) for a token.</td>
        </tr>
        <tr>
            <td rowspan="6"><a href="?ref_type=heads#coref">Coref</a></td>
            <td rowspan="6"><a href="?ref_type=heads#rb_coref"><tt>rb_coref</tt></a> (m)</td>
            <td rowspan="6">Lemmatizer<br/>Morphologizer<br/>Parser<br/> SpeakerExtractor</td>
            <td><tt>doc._.coref_clusters</tt> -> <tt>List[Cluster]</tt></td>
            <td>All clusters of corefering mentions in the document.</td>
        </tr>
        <tr>
            <td><tt>doc._.has_coref</tt> -> <tt>boolean</tt></td>
            <td>Whether any coreference has been resolved in the document.</td>
        </tr>
        <tr>
            <td><tt>span._.coref_cluster</tt> -> <tt>Cluster</tt></td>
            <td>Cluster of mentions that corefer with the span.</td>
        </tr>
        <tr>
            <td><tt>span._.is_coref</tt> -> <tt>boolean</tt></td>
            <td>Whether the span has at least one corefering mention.</td>
        </tr>
        <tr>
            <td><tt>token._.in_coref</tt> -> <tt>boolean</tt></td>
            <td>Whether the token is inside at least one corefering mention.</td>
        </tr>
        <tr>
            <td><tt>token._.coref_clusters</tt> -> <tt>List[Cluster]</tt></td>
            <td>All clusters of corefering mentions that contains the token.</td>
        </tr>
        <tr>
            <td><a href="?ref_type=heads#eventtagger">EventTagger</a></td>
            <td><a href="?ref_type=heads#neural_event_tagger"><tt>neural_event_tagger</tt></a> (m)</td>
            <td>Clausizer</td>
            <td><tt>span._.event</tt> -> <tt>Dict[str,Any]</tt></td>
            <td>Event annotations for the clause.</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="?ref_type=heads#formatter">Formatter</a></td>
            <td rowspan="2"><a href="?ref_type=heads#conllu_formatter"><tt>conllu_formatter</tt></a> (m)</td>
            <td rowspan="2"><i>Optional:</i><br/>DependencyParser</td>
            <td><tt>doc._.format_str</tt> -> <tt>str</tt></td>
            <td>The formatted document.</td>
        </tr>
        <tr>
            <td><tt>span._.format_str</tt> -> <tt>str</tt></td>
            <td>The formatted sentence (Span).</td>
        </tr>
        <tr>
            <td rowspan="4"><a href="?ref_type=heads#gentagger">GenTagger</a></td>
            <td rowspan="4"><a href="?ref_type=heads#flair_gen_tagger"><tt>flair_gen_tagger</tt></a> (m)<br/><br/><a href="?ref_type=heads#neural_gen_tagger"><tt>neural_gen_tagger</tt></a> (m)</td>
            <td rowspan="4">Clausizer</td>
            <td><tt>doc.spans['gi']</tt> -> <tt>SpanGroup</tt></td>
            <td>SpanGroup of generalising passages.</td>
        </tr>
        <tr>
            <td><tt>span._.gi</tt> -> <tt>Set[str]</tt></td>
            <td>Tags of a generalising passage.</td>
        </tr>
        <tr>
            <td><tt>token._.span['gi']</tt> -> <tt>Span</tt></td>
            <td>Shortest generalising passage containing the token.</td>
        </tr>
        <tr>
            <td><tt>token._.spans['gi']</tt> -> <tt>List[Span]</tt></td>
            <td>List of generalising passages containing the token.</td>
        </tr>
        <tr>
            <td rowspan="8"><a href="?ref_type=heads#normalizer">Normalizer</a></td>
            <td rowspan="8"><a href="?ref_type=heads#identity_normalizer"><tt>identity_normalizer</tt></a> (m)</td>
            <td rowspan="8">-</td>
            <td><tt>doc.text</tt> -> <tt>str</tt></td>
            <td>Normalised document's text.</td>
        </tr>
        <tr>
            <td><tt>doc._.text</tt> -> <tt>str</tt></td>
            <td>Original document's text.</td>
        </tr>
        <tr>
            <td><tt>token.idx</tt> -> <tt>int</tt></td>
            <td>Token's character offset in normalised document.</td>
        </tr>
        <tr>
            <td><tt>token._.idx</tt> -> <tt>int</tt></td>
            <td>Token's character offset in original document.</td>
        </tr>
        <tr>
            <td><tt>token.text</tt> -> <tt>str</tt></td>
            <td>Normalised token's text.</td>
        </tr>
        <tr>
            <td><tt>token._.text</tt> -> <tt>str</tt></td>
            <td>Original token's text.</td>
        </tr>
        <tr>
            <td><tt>token.whitespace_</tt> -> <tt>str</tt></td>
            <td>Normalised token's trailing whitespace.</td>
        </tr>
        <tr>
            <td><tt>token._.whitespace_</tt> -> <tt>str</tt></td>
            <td>Original token's trailing whitespace.</td>
        </tr>
        <tr>
            <td rowspan="4"><a href="?ref_type=heads#reflectiontagger">ReflectionTagger</a></td>
            <td rowspan="4"><a href="?ref_type=heads#neural_reflection_tagger"><tt>neural_reflection_tagger</tt></a> (m)</td>
            <td rowspan="4">Clausizer</td>
            <td><tt>doc.spans['rp']</tt> -> <tt>SpanGroup</tt></td>
            <td>SpanGroup of reflective passages.</td>
        </tr>
        <tr>
            <td><tt>span._.rp</tt> -> <tt>Set[str]</tt></td>
            <td>Tags of a reflective passage.</td>
        </tr>
        <tr>
            <td><tt>token._.span['rp']</tt> -> <tt>Span</tt></td>
            <td>Shortest reflective passage containing the token.</td>
        </tr>
        <tr>
            <td><tt>token._.spans['rp']</tt> -> <tt>List[Span]</tt></td>
            <td>List of reflective passages containing the token.</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="?ref_type=heads#semantictagger">SemanticTagger</a></td>
            <td rowspan="2"><a href="?ref_type=heads#germanet_semantic_tagger"><tt>germanet_semantic_tagger</tt></a> (m)</td>
            <td rowspan="2">Clausizer<br/>Lemmatizer</td>
            <td><tt>span._.verb_synset_id</tt> -> <tt>str</tt></td>
            <td>Root verb's SynSet ID.</td>
        </tr>
        <tr>
            <td><tt>token._.synset_id</tt> -> <tt>str</tt></td>
            <td>Token's SynSet ID.</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="?ref_type=heads#slicer">Slicer</a></td>
            <td rowspan="2"><a href="?ref_type=heads#from_start_slicer"><tt>from_start_slicer</tt></a> (m)</td>
            <td rowspan="2">-</td>
            <td><tt>doc._.fulltext</tt> -> <tt>str</tt></td>
            <td>The full text of the document.</td>
        </tr>
        <tr>
            <td><tt>doc.text</tt> -> <tt>str</tt></td>
            <td>The sliced text of the document.</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="?ref_type=heads#speakerextractor">SpeakerExtractor</a></td>
            <td rowspan="2"><a href="?ref_type=heads#rb_speaker_extractor"><tt>rb_speaker_extractor</tt></a> (m)</td>
            <td rowspan="2">DependencyParser<br/>Lemmatizer<br/>SpeechTagger<br/><br/><i>Optional:</i><br/>EntityRecognizer</td>
            <td><tt>span._.addressee</tt> -> <tt>Span</tt></td>
            <td>Addressee of a speech segment (Span).</td>
        </tr>
        <tr>
            <td><tt>span._.speaker</tt> -> <tt>Span</tt></td>
            <td>Speaker of a speech segment (Span).</td>
        </tr>
        <tr>
            <td rowspan="5"><a href="?ref_type=heads#speechtagger">SpeechTagger</a></td>
            <td rowspan="5"><a href="?ref_type=heads#flair_speech_tagger"><tt>flair_speech_tagger</tt></a> (m, c)<br/><br/><a href="?ref_type=heads#quotation_marks_speech_tagger"><tt>quotation_marks_speech_tagger</tt></a> (m)</td>
            <td rowspan="5">DependencyParser</td>
            <td><tt>doc.spans['speech']</tt> -> <tt>SpanGroup</tt></td>
            <td>SpanGroup of speech spans.</td>
        </tr>
        <tr>
            <td><tt>span._.speech</tt> -> <tt>Dict[str, float]</tt></td>
            <td>Probability of each speech type.</td>
        </tr>
        <tr>
            <td><tt>token._.span['speech']</tt> -> <tt>Span</tt></td>
            <td>Shortest speech segment containing the token.</td>
        </tr>
        <tr>
            <td><tt>token._.spans['speech']</tt> -> <tt>List[Span]</tt></td>
            <td>Speech segments containing the token.</td>
        </tr>
        <tr>
            <td><tt>token._.speech</tt> -> <tt>Dict[str, float]</tt></td>
            <td>Probability of each speech type.</td>
        </tr>
        <tr>
            <td rowspan="4"><a href="?ref_type=heads#temponymtagger">TemponymTagger</a></td>
            <td rowspan="4"><a href="?ref_type=heads#heideltime_temponym_tagger"><tt>heideltime_temponym_tagger</tt></a> (m)</td>
            <td rowspan="4">-</td>
            <td><tt>doc.spans['temponym']</tt> -> <tt>SpanGroup</tt></td>
            <td>SpanGroup of temponyms.</td>
        </tr>
        <tr>
            <td><tt>span._.temponym_norm</tt> -> <tt>Dict[str,str]</tt></td>
            <td>Norm value of a temponym (Span).</td>
        </tr>
        <tr>
            <td><tt>token._.span['temponym']</tt> -> <tt>Span</tt></td>
            <td>Shortest temponym containing the token.</td>
        </tr>
        <tr>
            <td><tt>token._.spans['temponym']</tt> -> <tt>List[Span]</tt></td>
            <td>Temponyms containing the token.</td>
        </tr>
        <tr>
            <td rowspan="4"><a href="?ref_type=heads#verbanalyzer">VerbAnalyzer</a></td>
            <td rowspan="4"><a href="?ref_type=heads#rb_verb_analyzer"><tt>rb_verb_analyzer</tt></a> (m)</td>
            <td rowspan="4">Clausizer<br/>Lemmatizer<br/>Morphologizer</td>
            <td><tt>span._.form</tt> -> <tt>MorphAnalysis</tt></td>
            <td>Verb form analysis of a clause (Span).</td>
        </tr>
        <tr>
            <td><tt>span._.form_main</tt> -> <tt>Token</tt></td>
            <td>Main verb of a clause (Span).</td>
        </tr>
        <tr>
            <td><tt>span._.form_modals</tt> -> <tt>List[Token]</tt></td>
            <td>Modal verbs of a clause (Span).</td>
        </tr>
        <tr>
            <td><tt>span._.form_verbs</tt> -> <tt>List[Token]</tt></td>
            <td>All verbs of a clause (Span).</td>
        </tr>
    </tbody>
</table>

\* The Sentencizer is included in the Parser.

\*\* The Tokenizer is strictly speaking not a *component* (i.e. a function that maps a `Doc` object to a `Doc` object).

Using `get_pipe_meta()`, you can always see which attributes are assigned to a document by the loaded pipeline components:

```python
for pipe_name in nlp.pipe_names:
    print(pipe_name, sorted(nlp.get_pipe_meta(pipe_name).assigns))
```

## Custom components/implementations in detail

### MONAPipe

#### AnnotationReader

The AnnotationReader adds annotations to a spaCy document.

##### `catma_annotation_reader`

**Requires:** –

The component `catma_annotation_reader` adds annotations made in [CATMA](https://catma.de/) to a spaCy document:

```python
import monapipe.pipeline.annotation_reader.catma_annotation_reader

nlp.add_pipe("catma_annotation_reader", config={"corpus_path": path_to_annotation_collection})
```

`catma_annotation_reader` requires the argument `corpus_path`, which is the path to the directory that contains the annotation collections. For example, if you use [MONACO](https://gitlab.gwdg.de/mona/korpus-public), you should set it to the root directory `korpus-public`.

The component will automatically search the correct annotation collections for the piped text. (Therefore it is necessary that the plain text file is stored next to the annotation file, as in a CATMA export.)

After piping a text, you can access all annotations in a document, or all annotations that overlap with a clause or token, with `doc._.annotations` or `token._.annotations`, respectively. The `annotations` attribute is of type `AnnotationList`. The key is the name of the annotation collection. The [`AnnotationList`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/blob/main/src/monapipe/annotation.py) stores the corresponding annotations ([`Annotation`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/blob/main/src/monapipe/annotation.py) objects):

```python
for anno in doc._.annotations:
    for annotation in doc._.annotations[anno]:
        print(annotation.tokens, annotation.tag)
```

#### AttributionTagger

The AttributionTagger labels clauses with attribution categories: `Figur` (*character*), `Erzählinstanz` (*narrator*), `Verdacht Autor` (*assumed author*).

##### `neural_attribution_tagger`

**Requires:** Clausizer

The `neural_attribution_tagger` uses the model from [Dönicke et al. (2022)](references.md).

```python
import monapipe.pipeline.attribution_tagger.neural_attribution_tagger

nlp.add_pipe("neural_attribution_tagger")
```

After piping a text, `clause._.attribution` contains the attributed speakers:

```python
for clause in doc._.clauses:
    print(clause, clause._.attribution)
```


#### Clausizer

The Clausizer segments each sentence of a document into clauses.

##### `dependency_clausizer`

**Requires:** DependencyParser

Clause segmentation can be performed with the `dependency_clausizer` from [Dönicke (2020)](references.md):

```python
import monapipe.pipeline.clausizer.dependency_clausizer

nlp.add_pipe("dependency_clausizer")
```

The default settings of the `dependency_clausizer` work for [UD](https://universaldependencies.org/u/dep/) parses. If you want to apply it on another parsing scheme, you can change the parameters as documented [here](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/blob/main/src/monapipe/pipeline/clausizer/dependency_clausizer.py). Note, however, that the `dependency_clausizer` was specifically designed for UD parses and yields significantly better results for them.

The clausizer adds clauses (`Span` objects) to the document, which can be accessed by `doc._.clauses`, `sent._.clauses` and `token._.clause`. Each clause has the following custom attributes, which each store a list of `Token` objects: `clause._.prec_punct` for preceding punctuation, `clause._.succ_punct` for succeeding punctuation and `clause._.tokens` for the tokens of a clause. Note that `clause._.tokens` and `list(clause)` may return differing lists of tokens, since a clause can be discontinuous but `Span` objects cannot.

```python
for sent in doc.sents:
    for clause in sent._.clauses:
        print(clause._.prec_punct)
        print(clause._.tokens)
        print(clause._.succ_punct)
```

#### Coref

The Coref component adds coreference clusters to a document.

##### `rb_coref`

**Requires:** DependencyParser, Morphologizer, SpeakerExtractor

The `rb_coref` component is based on the algorithm from [Krug et al. (2015)](references.md), which was extended by [Dönicke et al. (2022)](references.md) to create coreference clusters for all noun phrases in a text and not only character mentions.

```python
import monapipe.pipeline.coref.rb_coref

nlp.add_pipe("rb_coref")
```

After piping a text, you can access the [same custom attributes](https://github.com/huggingface/neuralcoref#Using-NeuralCoref) that are assigned by [NeuralCoref](https://github.com/huggingface/neuralcoref)'s custom spaCy component. (So in case they develop a German model in the future, you can replace our `coref` component by theirs without getting errors in follow-up components.)

```python
for coref_cluster in doc._.coref_clusters:
    print(coref_cluster.main, coref_cluster.mentions)
```

#### EntityRecognizer

The named entity recognizer (NER) fills the `doc.ents` attribute with entity spans. The entities are labelled with `PER`, `LOC`, `ORG` and `MISC`.

##### `bert_character_ner`

```python
import monapipe.pipeline.ner.bert_character_ner
nlp.add_pipe("bert_character_ner", config={"set_ents_mode": "s"})
```
The component implementation wraps a Bert model with a standard token classification head which was fine-tuned to detect characters nouns and named entities referencing a literary character.
More detailed information is available at [huggingface.co](https://huggingface.co/LennartKeller/fiction-gbert-large-droc-np-ner).

The `config` parameter `set_ents_mode` specifies how the new entities should be added with respect to existing entities in `doc.ents`. While adding the component implementation configure `set_ents_mode` as follows:

- "r" or "reset" (default): The new entities overwrite the existing entities.
- "s" or "substitute": The new entities substitute existing entities of the same label(s). Existing entities with other labels remain unchanged.
- "u" or "unify": The new entities are unified with the existing entities.

```python
for ent in doc.ents:
    print(character_span)

for idx, token in enumerate(doc):
    print(token, token.ent_iob_, token.ent_type_)
```

#### EventTagger

The EventTagger adds event labels to clauses.

##### `neural_event_tagger`

**Requires:** Clausizer

This component is a wrapper for the [event-classification](https://github.com/uhh-lt/event-classification/) model from [Vauth et al. (2021)](references.md):

```python
import monapipe.pipeline.event_tagger.neural_event_tagger

nlp.add_pipe("neural_event_tagger")
```

The tagger adds a custom `event` attribute to clauses, which is a dictionary with the following keys: `event_types`, `speech_type`, `thought_representation`, `iterative`, `mental`.

```python
for clause in doc._.clauses:
    print(clause, clause._.event)
```

#### Formatter

The Formatter converts the document and its sentences to a string representation.

##### `conllu_formatter`

The `conllu_formatter` converts the document to [CoNLL-U Plus Format](https://universaldependencies.org/ext-format.html). Per default, the format is basic [CoNLL-U Format](https://universaldependencies.org/format.html), but you can specify what should go into the table columns the config parameters (as documented [here](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/blob/main/src/monapipe/pipeline/formatter/conllu_formatter.py)). The following code adds a column with the NER type of a token:

```python
import monapipe.pipeline.formatter.conllu_formatter
from monapipe.pipeline.methods import serialize_config_param

column_names_plus = ["NER"]

column_funcs = {"NER" : lambda token: token.ent_type_}

nlp.add_pipe("conllu_formatter", config={"column_names_plus": column_names_plus, "column_funcs": serialize_config_param(column_funcs)})
```

You can access the string representations as follows:

```python
print(doc._.format_str)
for sent in doc.sents:
    print(sent._.format_str)
```

#### GenTagger

The GenTagger labels generalising passages in a document.

##### `flair_gen_tagger`

**Requires:** Clausizer

The `flair_gen_tagger` is a fast generalisation tagger that utilises the [FLAIR NLP framework](https://github.com/flairNLP/flair):

```python
import monapipe.pipeline.gen_tagger.flair_gen_tagger

nlp.add_pipe("flair_gen_tagger")
```

After piping a text, you can access all generalising passages in a document with `doc.spans['gi']`:

```python
for gi_span in doc.spans['gi']:
    print(gi_span, gi_span._.gi)
```

##### `neural_gen_tagger`

**Requires:** Clausizer

We also provide the neural tagger from [Schomacker et al. (2022)](references.md).

```python
import monapipe.pipeline.gen_tagger.neural_gen_tagger

nlp.add_pipe("neural_gen_tagger")
```

#### Normalizer

The Normalizer creates a new `Doc` object, where each word is replaced with its spelling-normalised form. The normalizer should be added before all other components in the pipeline.

##### `identity_normalizer`

The `identitiy_normalizer` does not normalise the any tokens. It is only useful to remove `SPACE` tokens:

```python
import monapipe.pipeline.normalizer.identity_normalizer

nlp.add_pipe("identity_normalizer", first=True, config={"remove_spaces": True})
```

After piping a text, the normalised form is accessible via `token.text`, the original form is preserved in `token._.text`; analogously for the character positions (`.idx`):

```python
for token in doc:
    print(token.text, token.idx, token._.text, token._.idx)
```

#### ReflectionTagger

Similarly to the GenTagger, the ReflectionTagger labels reflective passages (i.e. generalising passages, comment passages, or passages of non-fictional speech) in a document.

##### `neural_reflection_tagger`

**Requires:** Clausizer

The `neural_reflection_tagger` is a retrained version of the model from [Schomacker et al. (2022)](references.md).

```python
import monapipe.pipeline.reflection_tagger.neural_reflection_tagger

nlp.add_pipe("neural_reflection_tagger")
```

After piping a text, you can access all generalising passages in a document, token or clause with `doc.spans['rp']`, `token._.spans['rp']` or `clause.root._.spans['rp']`, respectively:

```python
for rp_span in doc.spans['rp']:
    print(rp_span, rp_span._.rp)
```

#### SemanticTagger

The SemanticTagger adds semantic categories to words and clauses.

##### `germanet_semantic_tagger`

**Requires:** Clausizer, Lemmatizer

The `germanet_semantic_tagger` used in [Weimer et al. (2022)](references.md) adds semantic categories from [GermaNet](http://www.sfs.uni-tuebingen.de/GermaNet/) [(Hamp & Feldweg, 1997)](references.md) to main verbs and adjectives, disambiguated in context. GermaNet uses [Levin (1993)](references.md)'s categories for verbs and [Hundsnurscher & Splett (1982)](references.md)'s categories for adjectives.

```python
import monapipe.pipeline.semantic_tagger.germanet_semantic_tagger

nlp.add_pipe("germanet_semantic_tagger")
```

After piping a text, `token._.synset_id` stores the ID of the GermaNet synset (if available). Additionally, `clause._.verb_synset_id` stores the ID of the clause's main verb. (`clause._.verb_synset_id` is equivalent to `clause.root._.synset_id`.) You can use the synset ID to look up the semantic category of the verb/adjective as follows:

```python
import monapipe.resource_handler as resources

germanet = resources.access("germanet")

for clause in doc._.clauses:
    if clause._.verb_synset_id is not None:
        print(clause.text, germanet.get_synset_by_id(clause._.verb_synset_id).word_class)
    for token in clause._.tokens:
        if token._.synset_id is not None:
            print(token.text, germanet.get_synset_by_id(token._.synset_id).word_class)
```

#### Slicer

The Slicer cuts out a slice from the document. The Slicer adds the custom attribute `doc._.fulltext`, which stores the original text, whereas `doc.text` is set to the sliced text.

##### `from_start_slicer`

If you only need to process the beginning of a text, you can use the `from_start_slicer`. For this, you have to set the `max_units` parameter to a positive integer.

Setting `units` to `"chars"`, `"sents"` or `"tokens"` cuts the text after the first characters (as in the original/unnormalised text, even if you use a Normalizer), sentences (as determined by the Sentencizer) or tokens, respectively.

```python
import monapipe.pipeline.slicer.from_start_slicer

nlp.add_pipe("from_start_slicer", after="sentencizer", config={"units" : "tokens", "max_units" : 100, "complete_sentences" : True})
```

The `complete_sentences` parameter determines whether the sentence that contains the last character or token should be completed.


#### SpeakerExtractor

The SpeakerExtractor extracts speaker and addressee for direct speech segments.

##### `rb_speaker_extractor`

**Requires:** DependencyParser, Lemmatizer, SpeechTagger

**Optional:** EntityRecognizer

The `rb_speaker_extractor` is very simple and can only extract explicitly mentioned speakers/addressees.

```python
import monapipe.pipeline.speaker_extractor.rb_speaker_extractor

nlp.add_pipe("rb_speaker_extractor")
```

After piping a text, you can iterate over the speech segments (`Span` objects) in a document and get the speaker and addressee (also `Span` objects).

```python
for speech_segment in doc.spans['speech']:
    print(speech_segment, speech_segment._.speaker, speech_segment._.addressee)
```

#### SpeechTagger

The component SpeechTagger identifies speech segments.

##### `flair_speech_tagger`

**Requires:** DependencyParser

The `flair_speech_tagger` is a wrapper for the [Redewiedergabe taggers](https://github.com/redewiedergabe/tagger) from [Brunner et al. (2020)](references.md), that utilise the [FLAIR NLP framework](https://github.com/flairNLP/flair):

```python
import monapipe.pipeline.speech_tagger.flair_speech_tagger

nlp.add_pipe("flair_speech_tagger")
```

The tagger adds a custom `speech` attribute to tokens and spans: `token._.speech` and `span._.speech`, both being of type `Dict[str,float]`. The keys of the dictionary are the speech types `"direct"`, `"freeIndirect"`, `"indirect"` and `"reported"`. For tokens, the value is the probability of the speech type. For spans, the value is the probability of the speech type averaged over tokens. All speech spans are stored in `doc.spans["speech"]`.

```python
for speech_span in doc.spans["speech"]:
    print(speech_span, speech_span._.speech)
    for token in speech_span:
        print(token, token._.speech)
```

##### `quotation_marks_speech_tagger`

**Requires:** –

Although the `flair_speech_tagger` provides scores for four different speech types, it also has two disadvantages: 1) it requires a comparatively long computation time, and 2) the models were trained on historical German texts and do not always yield satisfiable results. As an alternative, we provide the implementation `quotation_marks_speech_tagger`. This component only tags `"direct"` speech, but is comparatively fast and robust in doing so:

```python
from pipeline.components.speech_tagger import quotation_marks_speech_tagger

nlp.add_pipe("quotation_marks_speech_tagger")
```

#### TemponymTagger

The TemponymTagger extracts and normalises temporal expressions from a document.

##### `heideltime_temponym_tagger`

The original algorithm presented in [Strötgen & Gertz (2010)](references.md) was re-implemented for spaCy by [Barth & Dönicke (2021)](references.md), using the [German resource files](https://github.com/HeidelTime/heideltime/tree/master/resources/) from [HeidelTime](https://github.com/HeidelTime/heideltime).

```python
import monapipe.pipeline.temponym_tagger.heideltime_temponym_tagger

nlp.add_pipe("heideltime_temponym_tagger")
```

The temponyms (`Span` objects) in a text can be accessed through the document or the tokens:

```python
for temponym_span in doc.spans['temponym']:
    print(temponym_span.text, temponym_span._.temponym_norm)

for token in doc:
    if token._.span['temponym'] is not None:
        print(token.text, token._.span['temponym'])
```

The custom `temponym_norm` attribute is of type `Dict[str,str]` with the following keys (and possible values): `"NORM_MOD"` (END/MID/START), `"NORM_QUANT"` (EVERY), `"NORM_FREQ"` (1M/1S/1W), `"NORM_VALUE"` (*normalised time expression*), `"TYPE"` (date/duration/interval/set/time).

#### VerbAnalyzer

The VerbAnalyzer analyses the (complex) verb form of each clause.

##### `rb_verb_analyzer`

**Requires:** Clausizer, Lemmatizer, Morphologizer

This implementation combines different previous versions of the TMVM tagger from [Dönicke (2020, 2021, 2022)](references.md).

```python
import monapipe.pipeline.verb_analyzer.rb_verb_analyzer

nlp.add_pipe("rb_verb_analyzer")
```

After piping a text, each clause has a custom `form` attribute that stores grammatical analysis information of a clause as a `MorphAnalysis` object. To access an attribute of a clause, e.g. tense, use `clause._.form.get("Tense")`.

```python
for clause in doc._.clauses:
    print(clause, clause._.form, clause._.form_main, clause._.form_modals, clause._.form_verbs)
```

The custom attributes `form_main`, `form_modals` and `form_verbs` store the main verb, the modal verbs, and all verbs of the clause, respectively.

### External

This section provides some information how to load external component implementations.

#### EntityLinker

##### spacy-dbpedia-spotlight

Add the German model for [`spacy-dbpedia-spotlight`](https://github.com/MartinoMensio/spacy-dbpedia-spotlight) as follows:

```python
nlp.add_pipe("dbpedia_spotlight", config={"language_code": "de"})
```

Default attributes include `kb_id_` (URI of DBpedia), `label_` (always 'DBPEDIA_ENT'). Additionally, the custom attribute `dbpedia_raw_result` (raw json for the entity from DBpedia spotlight as Python Dict, keys: `@URI`, `@support`, `@types`, `@surfaceForm`, `@offset`, `@similarityScore``, `@percentageOfSecondRank`) is accessible:

```python
for ent_span in doc.ents:
    print(
        (
            ent_span.text,
            ent_span.kb_id_,
            ent_span.label_,
            ent_span._.dbpedia_raw_result
        )
    )
```
<!-- 
###### spacyopentapioca

Add the German model from [`spacyopentapioca`](https://github.com/UB-Mannheim/spacyopentapioca) as follows:

```python
nlp.add_pipe("opentapioca", config={"url": "https://opentapioca.wordlift.io/api/annotate?lc=de"})
```

Default attributes include `kb_id_` (Wikidata ID), `label_` (Wikidata label, e.g. 'PERSON' or 'LOC'). Custom attributes contain `description` (from Wikidata), `score` (linking score),  `types` (dict of relations to types such as Q43229 for organisation), Q618123 for geographic feature, Q5 for person, and more), and `aliases` (list of aliases in different languages for the entity label):

```
for ent_span in doc.ents:
    print(
        (
            ent_span.text, 
            ent_span.kb_id_, 
            ent_span.label_,
            ent_span._.description, 
            ent_span._.score, 
            ent_span._.types, 
            ent_span._.aliases
        )
    )
```

##### spacyfishing

Add the German model for [`spacyfishing`](https://github.com/Lucaterre/spacyfishing) as follows:

```python
nlp.add_pipe("entityfishing", config={"language": "de", "extra_info": True})
```

Default attributes include `kb_id_` (per default empty), `label_` (entity label, e.g. `PER`, `LOC`). 
Custom attributes contain `kb_qid` (Wikidata ID), `url_wikidata` (URL of Wikidata entry), `wikipedia_page_ref` (identifier of the Wikipedia concept), `nerd_score` (selection confidence score) and with config setting `"extra_info": True` also `normal_term` (normalised term name), `description` (short concept definition), `src_description` (Wikipedia source of description) and `other_ids` (statements in the KB related to a Wikipedia concept).

```python
for ent_span in doc.ents:
    print(
        (
            ent_span.text,
            ent_span.kb_id_,
            ent_span.label_,
            ent_span._.kb_qid,
            ent_span._.url_wikidata,
            ent_span._.wikipedia_page_ref,
            ent_span._.nerd_score,
            ent_span._.normal_term,
            ent_span._.description,
            ent_span._.src_description,
            ent_span._.other_ids
        )
    )
``` -->
