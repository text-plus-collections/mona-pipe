# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
import monapipe.pipeline.clausizer.dependency_clausizer
import monapipe.pipeline.semantic_tagger.germanet_semantic_tagger
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_germanet_semantic_tagger():
    """Integration test for the `germanet_semantic_tagger`."""
    pass
    # nlp = monapipe.model.load()
    # nlp.add_pipe("dependency_clausizer")
    # nlp.add_pipe("germanet_semantic_tagger")
    # doc = nlp(text_goethe_wv)
    # assert check_data_types(doc._.clauses, "verb_synset_id", str)
    # assert check_data_types(doc, "synset_id", str)
