# Integration of custom component and implementation


## Repository and package structure

 A pipeline component is a method that adds or modifies information in a `Doc` object. Specifically, it takes a `Doc` object as input and returns the same Doc object. New information added by the pipeline component is usually stored in new attributes, either attributes of the document or of spans or tokens within the document.

 For MONAPipe we set up a package architecture that provides pipeline components, such as `Normalizer`, `Clausizer`, `Coref`, or `SpeechTagger`, that assign a certain custom attribute to a spaCy object. Each instance can have different implementations for the same task. We structure pipeline components as subpackages that contain modules for the corresponding implementation. The component modules comply with the spaCy custom component architecture.

```text
mona-pipe
├── main
|   ├── container_test.py                   # script to manually launch/test only containerised implementations
|   └── example.py                          # script to manually launch/test all pipeline components
├── src
|   └── monapipe                            # monapipe package
|       └── pipeline                        # subpackage with source code of the pipeline
|           ├── custom_component_a          # subpackage with custom component a
|           ├── custom_component_b          # subpackage with custom component b
|           │   ├── custom_component.py     # module with super class for custom component
|           │   ├── methods.py              # module with methods used by multiple implementations
|           │   ├── implementation_a.py     # module with implementation a for custom component b
|           │   ├── implementation_b.py     # module with implementation b for custom component b
|           │   └── ...                    
|           └── ...   
└── tests                                   # integration tests package
    └── pipeline                            # subpackage with integration tests
        ├── test_implementation_a.py        # module with integration tests for implementation a
        ├── test_implementation_b.py        # module with integration tests for implementation b
        └── ...              
```

For example, MONAPipe has the custom component `SpeechTagger` which assigns the attribute `_.speech` to `Tokens` and `Span` objects (cf. [Table of components and implementations](../getting_started/component_overview.md#table-of-components-and-implementations)). For the component, we have set up the subpackage `monapipe/pipeline/speech_tagger` that includes two implementations corresponding to the the modules `quotation_marks_speech_tagger.py` (rule-based approach) and `flair_speech_tagger` (neuronal model). Additionally, `speech_tagger.py` contains the component class `SpeechTagger` that is used by both implementations. The module `methods.py` contains shared methods for both implementations.


## Custom component and implementation

With spaCy 3 custom components have to be registered via [`@Language.factory`](https://spacy.io/api/language#factory) decorator. For MONAPipe, we expand the specifications to the standard below.

Developers should generate the following files (documentation in `***` should be filled individually). If you need help with one of the steps, feel free to contact us!

1.) Provide a module `custom_component.py` (adapt the module naming for to your component) and specify the super class including spaCy attributes for your component.

Example:
```python
class ***CustomComponent***: 
    """The super class ***`CustomComponent`***."""

    assigns = {
        "doc.spans": "doc.spans['***your attribute***']",
        "span._.***your attribute***": "***your attribute***_span._.***your attribute***",
        "token._.***your attribute***": "token._.***your attribute***",
    }

    def __init__(self, nlp: Language, sentence_level: Optional[bool]):
        self.sentence_level = sentence_level

        # Attributes can be assigned to `Doc`, `Span`, or `Token`
        add_extension(Doc, "***your attribute***", {})
        add_extension(Span, "***your attribute***", {})
        add_extension(Token, "***your attribute***", {})
```

2.) Provide a module `implementation.py` (adapt the module naming for your implementation) with the following contents:

- A method that returns the custom implementation class corresponding including a decorator with necessary metadata (attribute assignements)
- The class containing your implementation with two methods: 
    - An `__init__` method that set requirements (other pipeline components that should have been loaded before) and the name of the attribute and its assignment level (`Doc`, `Span`, or `Token`).
    - A `__call__` method that contains your implementation code (eventually add more methods to structure your code).

If your component has multiple implementations that share certain methods, provide them in  an external module called `methods.py` within the component subpackage.

Example:
```python
@Language.factory(
    "***custom_implementation***",
    assigns=***CustomComponent***.assings,
    default_config={}
)
def ***custom_implementation***(nlp: Language, name: str):
    """Spacy pipeline component.
        ***Brief description.***

    Args:
        nlp: Spacy object.
        name: Implementation name based on decorator configuration.
    
    Returns:
        `Doc`: A spacy document object.

    Returns:
        ***`CustomImplementation`***.
    """

    return CustomImplementation(nlp)


class ***CustomImplementation***(CustomComponent):
    """The class ***`CustomImplementation`***."""

    def __init__(self, nlp: Language ***you might add additional variables***):
        requires(self, nlp, [***"parser"***])

        # Attributes can be assigned to `Doc`, `Span`, or `Token`
        add_extension(Doc, "attribute_name")
    
    def __call__(self, doc: Doc) -> Doc:

        # #################################
        # Insert your code for the current implementation.

        return doc
```

3.) Write an integration test in [`tests/pipeline`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/tests/pipeline) for your specific implementation; the module name should be the same as your implementation name, e.g. `test_implementation_a.py`.

Our method `check_data_types` checks that given spaCy objects store information of a certain data type.
Please provide the following arguments:
```text
objs: Iterable of spaCy objects, e.g. `[doc]` (for a single or multiple Docs), 
      `list(doc)` for all Tokens in a Doc.
name: Name of the spaCy attribute of these objects.
attr_type: The type that these spaCy attribute should have.
elem_type: If the attribute is an iterable, then the type which its element should have.
val_type: If the attribute is dictionary-like, then the type which its values should have.
```

Example:
```python
def test_implementation_a():
    """Integration test for `***implementation_a***`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("***implementation_a***")
    doc = nlp(text_goethe_wv)
    assert check_data_types(objs = ***doc***, 
                            name = "***attribute_name***", 
                            attr_type = ***dict***, 
                            elem_type = ***str***, 
                            val_type = ***float***)
    assert check_data_types(objs = doc.spans["***attribute_key***"], 
                            name = "***attribute_name***", 
                            attr_type= ***dict***, 
                            elem_type = ***str***, 
                            val_type = ***float***)
```

Also, add your component/implementation to [`main/example.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/blob/develop/main/example.py). This is a main method which runs each component and implementation.

4.) Update the documentation: Add your component and/or implementation to the [Table of components and implementations](../getting_started/component_overview.md#table-of-components-and-implementations) and write a paragraph in the section [Custom components/implementations in detail/MONAPipe](../getting_started/component_overview.md#monapipe) that describes your component/implementation and gives a brief code example.


## Integration of external resources

Some components like automatic classifiers need external resources such as machine learning models. These resources often have a large file size and have to be stored outside of the repository. If your model is provided from external storages (such as Hugging Face Hub) you can add them from there.

Otherwise, we provide an integration to the long-term archive [Gro.data](https://data.goettingen-research-online.de/), which is an open-source dataverse. The following steps are mandatory to integrate the Gro.data ressources to MONAPipe.

1. Request for access to the dataverse [MONAPipe Data](https://data.goettingen-research-online.de/dataverse/mona-pipe-data).
2. Add a dataset within the dataverse MONAPipe-Data that contains your model and all necessary metadata for the publication.
3. Integrate your model within the MONAPipe repository:
    - Add a subpackage in `monapipe/resources` likely with a name corresponding to your component (don't forget the `__init__` file).
    - Add a ``load.py`` module within your subpackage. This module should perform any necessary steps to provide your component with the data from the resource and has to be individually set up. We provide a `ResourceHandler` class that downloads your model from Gro.data (have a look at existing load modules of other resources for its usage or ask us). 
    For the functionality of the `ResourceHandler` you have to provide the DOI of your dataset given by Gro.data and store it in the global variable `DATAVERSE` in `monapipe/config.py` (please name the key to the dictionary `"doi_{resource_subpackage_name}"`). You can work with unpublished datasets by specifying the draft version of you dataset: `"doi:xx.xxxxx/xxxxxx&version=DRAFT"`; for this, you additionally have to set your personal API token in the `DATAVERSE` variable (do never commit the API token).
4. Publish the dataset and change the DOI in `config.py` to the current version, e.g. `"doi:xx.xxxxx/xxxxxx&version=1.0"`. Don't forget the version, otherwise, Gro.data will always deliver the newest version which might compromise the functionality of older versions of MONAPipe.
