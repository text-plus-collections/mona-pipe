# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
import monapipe.pipeline.coref.rb_coref
import monapipe.pipeline.speaker_extractor.rb_speaker_extractor
import monapipe.pipeline.speech_tagger.quotation_marks_speech_tagger
from monapipe.neuralcoref import Cluster
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_rb_coref():
    """Integration test for the `rb_coref`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("quotation_marks_speech_tagger")
    nlp.add_pipe("rb_speaker_extractor")
    nlp.add_pipe("rb_coref")
    doc = nlp(text_goethe_wv[:10000])
    assert check_data_types([doc], "coref_clusters", list, Cluster)
    assert check_data_types([doc], "coref_resolved", str)
    assert check_data_types([doc], "coref_scores", dict)
    assert check_data_types([doc], "has_coref", bool)
    assert check_data_types(list(doc.ents) + list(doc.noun_chunks), "coref_cluster", Cluster)
    assert check_data_types(list(doc.ents) + list(doc.noun_chunks), "coref_scores", dict)
    assert check_data_types(list(doc.ents) + list(doc.noun_chunks), "is_coref", bool)
    assert check_data_types(doc, "coref_clusters", list, Cluster)
    assert check_data_types(doc, "in_coref", bool)
