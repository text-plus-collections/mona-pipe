# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

AUXILIARY_VERBS = {
    "AUX1": "haben|sein",  # perfect aspect
    "AUX2": "sein|werden",  # passive voice
    "AUX3": "werden",  # future tense
    "AUX4": "sein",  # copula
}
