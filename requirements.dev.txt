# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

bandit==1.7.9
black==24.8.0
build==1.2.1
commitizen==3.29.0
gitlabci-local==10.2.0
isort==5.13.2
mkdocs==1.6.1
mkdocs-include-markdown-plugin==6.2.2
mkdocs-jupyter==0.24.8
mypy==1.11.2
pre-commit==3.8.0
pylint==3.2.7
pylintfileheader==1.0.0
pytest==8.3.2
pytest-xdist==3.6.1
python-semantic-release==9.8.8
reuse==4.0.3
tox==4.18.0
twine==5.1.1
