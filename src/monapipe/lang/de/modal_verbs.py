# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

MODAL_VERBS = set(["dürfen", "können", "mögen", "müssen", "sollen", "wollen"])
