# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

from typing import Iterable


def check_data_types(
    objs: Iterable, name: str, attr_type: type, elem_type: type = None, val_type: type = None
) -> bool:
    """Test whether the given spacy objects store information of a certain data type.
        Use this method for pipeline component tests to check if attributes have been properly set.

    Args:
        objs: Iterable of spacy objects.
        name: Name of the attribute of these objects.
        attr_type: The type that these attributes should have.
        elem_type: If the attributes are iterables, then the type which their elements should have.
        val_type: If the attributes are dictionary-like, then the type which their values should have.

    Returns:
        False if there are any type mismatches or no values/elements/attributes at all; ignoring Nones.
            True otherwise.

    """
    # Move from objects to custom extensions.
    objs = [obj._ for obj in objs if hasattr(obj, "_")]

    if len(objs) == 0:
        return False

    # Only keep objects that have the attribute.
    objs = [obj for obj in objs if hasattr(obj, name) and getattr(obj, name) is not None]

    if len(objs) == 0:
        return False

    # Get the attributes from all objects.
    attrs = [getattr(obj, name) for obj in objs]

    if len(attrs) == 0:
        return False

    for attr in attrs:
        if not isinstance(attr, attr_type):
            return False

    if elem_type is not None:  # the attributes are iterables
        # Get the elements from all attributes.
        elems = [elem for attr in attrs for elem in attr if elem is not None]

        if len(elems) == 0:
            return False

        for elem in elems:
            if not isinstance(elem, elem_type):
                return False

    if val_type is not None:  # the attributes are dictionaries
        # Get the values from all attributes.
        vals = [attr[elem] for attr in attrs for elem in attr if attr[elem] is not None]

        if len(vals) == 0:
            return False

        for val in vals:
            if not isinstance(val, val_type):
                return False

    return True
