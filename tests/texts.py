# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import os

text_goethe_wv_full = open(
    os.path.join(
        os.path.dirname(__file__),
        "texts",
        "Goethe__Die_Wahlverwandtschaften",
        "Goethe__Die_Wahlverwandtschaften.txt",
    )
).read()

text_goethe_wv_clip = text_goethe_wv_full[:1224]

# determine whether the tests should run on
# `text_goethe_wv_full` or `text_goethe_wv_clip`
text_goethe_wv = text_goethe_wv_clip
