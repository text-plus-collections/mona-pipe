# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

SPEECH_VERBS = set(
    [
        "antworten",
        "äußern",
        "blaffen",
        "brüllen",
        "denken",
        "erwidern",
        "erzählen",
        "flüstern",
        "fragen",
        "glauben",
        "meinen",
        "mutmaßen",
        "reden",
        "rufen",
        "sagen",
        "schreien",
        "seufzen",
        "singen",
        "sprechen",
        "vermuten",
        "weinen",
        "wispern",
    ]
)
