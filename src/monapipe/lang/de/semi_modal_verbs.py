# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

SEMI_MODAL_VERBS = set(
    ["drohen", "lassen", "pflegen", "scheinen", "vermögen", "versprechen", "wissen"]
)
