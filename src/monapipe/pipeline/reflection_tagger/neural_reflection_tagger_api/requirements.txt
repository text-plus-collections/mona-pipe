# api
fastapi<=0.115.7
pydantic<=2.10.6
uvicorn[standard]<=0.34.0

# neural_reflection_tagger
pandas<=2.2.3
scikit-learn<=1.6.1
torch<=2.5.1
transformers<=4.48.1