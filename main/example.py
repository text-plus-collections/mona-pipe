# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import os
import pickle
import random

import monapipe.model
import monapipe.pipeline.annotation_reader.catma_annotation_reader
import monapipe.pipeline.attribution_tagger.neural_attribution_tagger
import monapipe.pipeline.clausizer.dependency_clausizer
import monapipe.pipeline.coref.rb_coref
import monapipe.pipeline.event_tagger.neural_event_tagger
import monapipe.pipeline.formatter.conllu_formatter
import monapipe.pipeline.gen_tagger.flair_gen_tagger
import monapipe.pipeline.gen_tagger.neural_gen_tagger
import monapipe.pipeline.ner.bert_character_ner
import monapipe.pipeline.normalizer.identity_normalizer
import monapipe.pipeline.reflection_tagger.neural_reflection_tagger
import monapipe.pipeline.semantic_tagger.germanet_semantic_tagger
import monapipe.pipeline.slicer.from_start_slicer
import monapipe.pipeline.speaker_extractor.rb_speaker_extractor
import monapipe.pipeline.speech_tagger.flair_speech_tagger
import monapipe.pipeline.temponym_tagger.heideltime_temponym_tagger
import monapipe.pipeline.verb_analyzer.rb_verb_analyzer
import monapipe.resource_handler as resources
from monapipe.pickling import make_pickleable, unmake_pickleable
from monapipe.pipeline.methods import serialize_config_param

if __name__ == "__main__":
    # path to the folder that contains the text to read in
    text_path = os.path.join(os.path.dirname(__file__), "..", "tests", "texts")

    # read in the text
    text_goethe_wv = open(
        os.path.join(
            text_path, "Goethe__Die_Wahlverwandtschaften", "Goethe__Die_Wahlverwandtschaften.txt"
        )
    ).read()

    # define the columns for the CoNLL output
    column_names = [
        "ID",
        "FORM",
        "NORM",
        "LEMMA",
        "UPOS",
        "CASE",
        "HEAD",
        "DEPREL",
        "CLAUSE",
        "TMV",
        "EVENT",
        "SPEECH",
        "SPEAKER",
        "ENT",
        "WIKI",
        "COREF",
        "TEMP",
        "SEM",
        "GI",
        "RP",
        "ATTR",
        "ANNO",
    ]

    # define the functions for the CoNLL output (only needed for non-standard fields)
    column_funcs = {
        "NORM": lambda token: token.text,
        "CASE": lambda token: ",".join(token.morph.get("Case")),
        "CLAUSE": lambda token: token.sent._.clauses.index(token._.clause),
        "TMV": lambda token: ",".join(token._.clause._.form.get("Tense")),
        "EVENT": lambda token: token._.clause._.event["event_types"],
        "SPEECH": lambda token: ",".join(token._.speech.keys()),
        "SPEAKER": lambda token: token._.span["speech"]._.speaker,
        "ENT": lambda token: (token.ent_iob_ + "-" + token.ent_type_),
        "WIKI": lambda token: token.ent_kb_id_,
        "COREF": lambda token: ",".join([str(cluster.i) for cluster in token._.coref_clusters]),
        "TEMP": lambda token: token._.span["temponym"]._.temponym_norm["NORM_VALUE"],
        "SEM": lambda token: str(
            resources.access("germanet").get_synset_by_id(token._.synset_id).word_class
        ).replace("WordClass.", ""),
        "GI": lambda token: ",".join([tag for span in token._.spans["gi"] for tag in span._.gi]),
        "RP": lambda token: ",".join([tag for span in token._.spans["rp"] for tag in span._.rp]),
        "ATTR": lambda token: ",".join(token._.clause._.attribution),
        "ANNO": lambda token: ",".join(token._.annotations["GGG"].get_tags()),
    }

    # build the pipeline
    nlp = monapipe.model.load()
    nlp.add_pipe("identity_normalizer", first=True)
    nlp.add_pipe("from_start_slicer", config={"max_units": 8})
    nlp.add_pipe("bert_character_ner", config={"set_ents_mode": "s"})
    nlp.add_pipe("dbpedia_spotlight", config={"language_code": "de"})
    # nlp.add_pipe(
    #     "opentapioca", config={"url": "https://opentapioca.wordlift.io/api/annotate?lc=de"}
    # )
    # nlp.add_pipe("entityfishing", config={"language": "de", "extra_info": True})
    nlp.add_pipe("dependency_clausizer")
    nlp.add_pipe("rb_verb_analyzer")
    nlp.add_pipe("neural_event_tagger")
    nlp.add_pipe("flair_speech_tagger")
    nlp.add_pipe("rb_speaker_extractor")
    nlp.add_pipe("rb_coref")
    nlp.add_pipe("heideltime_temponym_tagger")
    # nlp.add_pipe("germanet_semantic_tagger") # activate if you provide Germanet
    nlp.add_pipe(random.choice(["flair_gen_tagger", "neural_gen_tagger"]))
    nlp.add_pipe("neural_reflection_tagger")
    nlp.add_pipe("neural_attribution_tagger")
    nlp.add_pipe("catma_annotation_reader", config={"corpus_path": text_path})
    nlp.add_pipe(
        "conllu_formatter",
        config={
            "column_names": column_names,
            "column_funcs": serialize_config_param(column_funcs),
            "delimiter": r"\s+",
        },
    )

    # print the attributes that are assigned by pipeline components
    for pipe_name in nlp.pipe_names:
        try:  # works for custom components
            print(pipe_name, sorted(nlp.get_pipe_meta(pipe_name).assigns.values()))
        except AttributeError:  # works for any components
            print(pipe_name, sorted(nlp.get_pipe_meta(pipe_name).assigns))

    # pipe the text
    doc = nlp(text_goethe_wv)

    # saving and loading
    doc_data = pickle.dumps(make_pickleable(doc))
    doc = unmake_pickleable(pickle.loads(doc_data))

    print(doc._.format_str)
