# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
from tests.texts import text_goethe_wv


def test_dbpedia_spotlight():
    """Integration test for the external component `dbpedia_spotlight`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("dbpedia_spotlight", config={"language_code": "de"})
    doc = nlp(text_goethe_wv)
    for ent_span in doc.ents:
        print(
            f"Entity: {ent_span.text}, "
            f"kb_id: {ent_span.kb_id_}, "
            f"Label: {ent_span.label_}, "
            f"dbpedia_raw_result: {ent_span._.dbpedia_raw_result} "
        )
