# Containerised implementation


## API creation and integration

For implementations that use several dependencies (e.g. different NLP libraries), it is highly recommended to build a containerised API that delivers results of the implementation and integrate it into MONAPipe. We provide a guide for a containerisation via Docker.

API integration can be highly individual. Therefore, we show the integration of `flair_speech_tagger`, one of the implementations of the component `speech_tagger`, as an example.

```text
mona-pipe
├── main
|   └── container_test.py                               # script to manually launch/test only containerised implementations
├── src
|   └── monapipe                            
|       ├── pipeline                        
|       |   └── speech_tagger            
|       |       ├── speech_tagger.py     
|       |       ├── methods.py              
|       |       ├── flair_speech_tagger.py       
|       |       ├── ...  
|       |       └── flair_speech_tagger_api             # subpackage with docker config and fast api                  
|       |           ├── app                             # subpackage with fast api
|       |           |   ├── resources
|       |           |   |   └── speech_taggers
|       |           |   |       └── load.py
|       |           |   ├── config.py
|       |           |   ├── flair_speech_tagger_api.py
|       |           |   ├── main.py
|       |           |   └── resource_handler.py
|       |           ├── config.py
|       |           ├── Dockerfile   
|       |           └── requirements.txt
|       ├── config.py
|       └── docker.py
└── tests                                               # integration tests package
    └── pipeline-container                              # subpackage with integration tests for containerised implementations
        └── test_flair_speech_tagger.py                 # module with integration tests

```

First, add a subpackage named after the implementation extended by the suffix `_api`, in our case [`flair_speech_tagger_api`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api).

Create the following contents within that subpackage (use our examples as orientation):

- A [`Dockerfile`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/Dockerfile). Please adjust the container port with the setting you set in `config.py` below.
- A [`requirements.txt`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/requirements.txt) including all Python dependencies for the API, such as API dependencies (fastapi, pydantic) and all packages used by your implementation (set them to determinded versions as much as possible to guarantee long time functionality).

Within the subpackage [`flair_speech_tagger_api`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api) create another subpackage [`app`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/app/) for the fast api setup:

- A module [`main.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/app/main.py) that contains the fast api setup and sets the endpoint `flair_speech_tagger_api`.
- A module [`request_flair_speech_tagger.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/app/request_flair_speech_tagger.py) that provides and launches the tagger and delivers it's results when requested.
- The subpackage [`resources`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/app/resources/) is another a mirror from the main package [`monapipe`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe). It contains the same subpackage structure (see depiction above) with a module `load.py` that loads all models for the corresponding tagger.

Lastly, integrate your API in the superordinate framework of MONAPipe. We provide a Pythonic integration via `docker` library that builds image and container for your API, and delivers it through the dedicated port. This is setup in the module [`docker.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/docker.py) (no need for adjustments).

- Add a `container_port` (should match with the port in the `Dockerfile`) and a `host_port` for your API implementation in [`config.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/config.py) at the top level of the `monapipe` package.
- Integrate the API in your actual implementation (in our case the implementation module [`flair_speech_tagger.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/pipeline/speech_tagger/flair_speech_tagger.py)).
    - First, load the API every time the implementation is added to a spaCy pipeline via `add_pipe`. Therefore, update `default_config` of the implementation class (in our example `FlairSpeechTagger`) with parameters for `docker` and the `api_mode`. The parameter `docker` let's you the Dockerfile. `api_mode` currently is limited to `"localhost"`, which utilises the Docker setup. The integration of an online API without dependency of local containers is planned. Next, import the port from [`config.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/config.py), store it as class attribute, and, finally, run the method `provide_docker_container` from the module [`docker.py`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/docker.py).
    - Instead of calling your tagger directly, request the API and append the results to the custom attribute you have defined. In our example, we have done this in the method `_add_speech_tags_to_tokens_api` of our implementation. 


## Test workflow

- As always write tests for your implementation, in this case in the subpackage [`tests/pipeline-container`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/tests/pipeline-container). When adding your implementation to the spaCy pipeline append the parameter `config={"api_mode": "service"}`, e.g.: `nlp.add_pipe("flair_speech_tagger", config={"api_mode": "service"})`. This will trigger the right configuration for the gitlab-ci.
- Update the [`container_test`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/src/monapipe/main/container_test.py) script for quick local checkups.
- Adjust the [`.gitlab-ci.yml`](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/blob/develop/.gitlab-ci.yml) to run your container as service in Gitlab:
    - Stage `container build`: Add your container with the 3 variables `COMPONENT_NAME`, `CONTEXT_PATH`, and `DOCKERFILE_PATH` (see how previous containers are specified) to the `parallel`/`matrix` configuration. In `before_script`, add two `cp` commands to copy the files `config.py` and `ressource_handler.py` in the `app` folder of your container API implementation, e.g.:
        - `cp src/monapipe/config.py src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/app`
        - `cp src/monapipe/resource_handler.py src/monapipe/pipeline/speech_tagger/flair_speech_tagger_api/app`
    - Stage `container tests` and stage `integration tests`: Add your container below `services`. Specify `alias` (pay attention: alias cannot contain underscores, use hyphen instead) and the name of the harbor image in the format `harbor.gwdg.de/sub-fe-pub/${CI_PROJECT_PATH}/new_container_name_with_underscores:${CI_COMMIT_SHORT_SHA}`.
    - Stage `container tests`: Update the `SERVICE_ALIASES` variable. Seperate services by space (will be interpreted as array) such as `SERVICE_ALIASES="flair-speech-tagger-api new-service-with-hyphens"`.
    
