# References

## Main reference

> Tillmann Dönicke, Florian Barth, Hanna Varachkina, and Caroline Sporleder (2022). [MONAPipe: Modes of Narration and Attribution Pipeline for German Computational Literary Studies and Language Analysis in spaCy](https://aclanthology.org/2022.konvens-1.2/). In Proceedings of the 18th Conference on Natural Language Processing (KONVENS 2022).

## Other references

> Florian Barth and Tillmann Dönicke (2021). [Participation in the KONVENS 2021 Shared Task on Scene Segmentation Using Temporal, Spatial and Entity Feature Vectors](http://ceur-ws.org/Vol-3001/paper4.pdf). In Proceedings of the Shared Task on Scene Segmentation.

> Annelen Brunner, Ngoc Duyen Tanja Tu, Lukas Weimer, Fotis Jannidis (2020). [To BERT or not to BERT – Comparing contextual embeddings in a deep learning architecture for the automatic recognition of four types of speech, thought and writing representation](http://ceur-ws.org/Vol-2624/paper5.pdf). In Proceedings of the 5th Swiss Text Analytics Conference (SwissText) & 16th Conference on Natural Language Processing (KONVENS).

> Tillmann Dönicke (2020). [Clause-Level Tense, Mood, Voice and Modality Tagging for German](https://aclanthology.org/2020.tlt-1.1.pdf). In Proceedings of the 19th Workshop on Treebanks and Linguistic Theories.

> Tillmann Dönicke (2021). [Delexicalised Multilingual Discourse Segmentation for DISRPT 2021 and Tense, Mood, Voice and Modality Tagging for 11 Languages](https://aclanthology.org/2021.disrpt-1.4.pdf). In Proceedings of the 2nd Shared Task on Discourse Relation Parsing and Treebanking (DISRPT 2021).

> Tillmann Dönicke (2022). [Rule-Based Clause-Level Morphology for Multiple Languages](https://aclanthology.org/2022.mrl-1.5.pdf). In Proceedings of the 2nd Multilingual Representation Learning Workshop (MRL 2022).

> Tillmann Dönicke (2023): [German UD spaCy model (md)](https://doi.org/10.25625/S2LPJP), GRO.data, V1.

> Tillmann Dönicke, Hanna Varachkina, Anna M. Weimer, Luisa Gödeke, Florian Barth, Benjamin Gittel, Anke Holler, and Caroline Sporleder (2022). [Modelling Speaker Attribution in Narrative Texts With Biased and Bias-Adjustable Neural Networks](https://www.frontiersin.org/articles/10.3389/frai.2021.725321/full). Frontiers in Artificial Intelligence.

> Birgit Hamp and Helmut Feldweg (1997). [GermaNet – a Lexical-Semantic Net for German](https://www.aclweb.org/anthology/W97-0802.pdf). Automatic information extraction and building of lexical semantic resources for NLP applications.

> Franz Hundsnurscher and Jochen Splett (1982). *Semantik der Adjektive des Deutschen. Analyse der semantischen Relationen*.

> Markus Krug, Frank Puppe, Fotis Jannidis, Luisa Macharowsky, Isabella Reger, and Lukas Weimar (2015). [Rule-based Coreference Resolution in German Historic Novels](https://aclanthology.org/W15-0711.pdf). In Proceedings of the Fourth Workshop on Computational Linguistics for Literature.

> Beth Levin (1993). *English verb classes and alternations: A preliminary Investigation*.

> Thorben Schomacker, Tillmann Dönicke, and Marina Tropmann-Frick (2022). [Automatic Identification of Generalizing Passages in German Fictional Texts using BERT with Monolingual and Multilingual Training Data](https://zenodo.org/record/6979859). Extended abstract submitted and accepted for the KONVENS 2022 Student Poster Session.

> Jannik Strötgen and Michael Gertz (2010). [Heideltime: High quality rule-based extraction and normalization of temporal expressions](https://www.aclweb.org/anthology/S10-1071.pdf). In Proceedings of the 5th International Workshop on Semantic Evaluation.

> Hanna Varachkina, Florian Barth, Luisa Gödeke, Anna Mareike Hofmann, and Tillmann Dönicke (2022). [Reflexive Passagen und ihre Attribution](https://doi.org/10.5281/zenodo.6328207). DHd 2022 Kulturen des digitalen Gedächtnisses. 8. Tagung des Verbands "Digital Humanities im deutschsprachigen Raum" (DHd 2022), Potsdam.

> Michael Vauth, Hans Ole Hatzel, Evelyn Gius, and Chris Biemann (2021). [Automated Event Annotation in Literary Texts](http://ceur-ws.org/Vol-2989/short_paper18.pdf). Computational Humanities Research Conference (CHR 2021).

> Anna Mareike Weimer, Florian Barth, Tillmann Dönicke, Luisa Gödeke, Hanna Varachkina, Anke Holler, Caroline Sporleder, and Benjamin Gittel (to appear). [The (In-)Consistency of Literary Concepts. Operationalising, Annotating and Detecting Literary Comment](https://jcls.io/article/id/90/). Journal of Computational Literary Studies.