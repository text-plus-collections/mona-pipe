# Licenses

Original parts are licensed under LGPL-3.0-or-later. Derivative code and integretated resources are licensed under the respective license of the original (see table below). Documentation, configuration and generated code files are licensed under CC0-1.0.

## Licenses of resources

| resource                                  | licence                                       |
| ----------------------------------------- | --------------------------------------------- |
| attribution                               | CC BY 4.0                                     |
| event_classification                      | MIT                                           |
| generalizing_passages_identification_bert | MIT                                           |   
| heideltime                                | GPL-3.0                                       |
| open_multilingual_wordnet                 | CC BY SA 3.0, FDL 1.1 (cf. [open_multilingual_wordnet_licence_statement.txt](LICENSES/open_multilingual_wordnet_licence_statement.txt)) |
| reflective_passages_identification_bert   | MIT                                           |
| spacy_model                               | CC BY-SA 3.0, CC BY-SA 4.0, CC-BY-NC-SA 4.0   |
| speech_taggers: flair_speech_tagger       | bsd-2-clause                                  |