# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
from tests.texts import text_goethe_wv


def test_entityfishing():
    """Integration test for the external component `entityfishing`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("entityfishing", config={"language": "de", "extra_info": True})
    doc = nlp(text_goethe_wv)
    for ent_span in doc.ents:
        print(
            f"Entity: {ent_span.text}, "
            f"kb_id: {ent_span.kb_id_}, "
            f"Label: {ent_span.label_}, "
            f"kb_qid: {ent_span._.kb_qid}, "
            f"url_wikidata: {ent_span._.url_wikidata}, "
            f"wikipedia_page_ref: {ent_span._.wikipedia_page_ref}, "
            f"nerd_score: {ent_span._.nerd_score}, "
            f"normal_term: {ent_span._.normal_term}, "
            f"description: {ent_span._.description}, "
            f"src_description: {ent_span._.src_description}, "
            f"other_ids: {ent_span._.other_ids} "
        )
