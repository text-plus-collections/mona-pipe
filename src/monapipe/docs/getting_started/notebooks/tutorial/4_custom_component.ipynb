{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to: Build a custom component\n",
    "\n",
    "This notebook aids you through creating a simple custom component which you can add to MONAPipe. Your component will be able to detect the perspective of a text, i.e. whether the text has a 1st-, 2nd-, or 3rd-person narrator.\n",
    "\n",
    "For example, the following text (which is the beginning of August Bürger's *Münchhausen*) has a 1st-person narrator, which can be clearly seen by the frequent use of the 1st-person pronoun *Ich*:\n",
    "\n",
    "*<u>Ich</u> trat meine Reise nach Rußland von Haus ab mitten im Winter an, weil <u>ich</u> ganz richtig schloß, daß Frost und Schnee die Wege durch die nördlichen Gegenden von Deutschland, Polen, Kur- und Livland, welche nach der Beschreibung aller Reisenden fast noch elender sind als die Wege nach dem Tempel der Tugend, endlich, ohne besondere Kosten hochpreislicher, wohlfürsorgender Landesregierungen, ausbessern müßte. <u>Ich</u> reisete zu Pferde, welches, wenn es sonst nur gut um Gaul und Reiter steht, die bequemste Art zu reisen ist. Denn man riskiert alsdann weder mit irgendeinem höflichen deutschen Postmeister eine Affaire d'honneur zu bekommen, noch von seinem durstigen Postillion vor jede Schenke geschleppt zu werden.*"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before you can start, you will need some imports:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9f2d9ded",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/home/fbarth/GIT/mona-pipe/env_dev/lib/python3.8/site-packages/tqdm/auto.py:21: TqdmWarning: IProgress not found. Please update jupyter and ipywidgets. See https://ipywidgets.readthedocs.io/en/stable/user_install.html\n",
      "  from .autonotebook import tqdm as notebook_tqdm\n"
     ]
    }
   ],
   "source": [
    "# import pipeline components\n",
    "import monapipe.model\n",
    "from monapipe.pipeline.methods import add_extension, requires\n",
    "\n",
    "# import spaCy classes\n",
    "from spacy.language import Language\n",
    "from spacy.tokens import Doc, Span, Token\n",
    "\n",
    "# import TextGrid components\n",
    "from tgclients.aggregator import Aggregator\n",
    "aggregator = Aggregator()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "7459bc51",
   "metadata": {},
   "source": [
    "A pipeline component is a method that adds or modifies information in a `Doc` object. Specifically, it takes a `Doc` object as input and returns the same `Doc` object. New information added by the pipeline component is usually stored in new attributes, either attributes of the document or of spans or tokens within the document.\n",
    "\n",
    "With spaCy 3 custom components have to be registered via the [`@Language.factory`](https://spacy.io/api/language#factory) decorator. For MONAPipe, the specifications are expanded in the cell below.\n",
    "\n",
    "Here, a component super class `PerspectiveTagger` is defined including spaCy attributes for the component. The `__init__` method of the class, uses the method `add_extension` to initialise a new custom attribute. The usuage is illustrated below, where `add_extension(Doc, \"perspective\")` is called to define the custom attribute `\"perspective\"` for `Doc` objects (it works likewise for `Span` and `Token` objects). From now on, `doc._.perspective` is accessible (the value is `None` per default). `add_extension` internally calls spaCy's `set_extension` method but is a bit more convenient to use (e.g. because `set_extension` can only be called outside of pipeline components/implementations).\n",
    "\n",
    "Next, the name for the new component implementation is defined (`\"rb_perspective_tagger\"`, `rb_` stands for *rule-based*), and the attributes are provided via the component super class. \n",
    "\n",
    "In the following step, the class `RbPerspectiveTagger` is constructed for the component implementation. This implementation class inherits attributes from the component super class and the `__init__` method further defines required preceding pipeline components via `requires`, in this case the `parser`. The `call` method contains the implementation code.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "cff23dfb",
   "metadata": {},
   "outputs": [],
   "source": [
    "class PerspectiveTagger:\n",
    "    \"\"\"Component super class `PerspectiveTagger`.\"\"\"\n",
    "\n",
    "    assigns = {\"doc._.perspective\": \"doc._.perspective\"}\n",
    "\n",
    "    def __init__(self, nlp: Language):\n",
    "        add_extension(Doc, \"perspective\", {})\n",
    "\n",
    "\n",
    "@Language.factory(\n",
    "    \"rb_perspective_tagger\",\n",
    "    assigns=PerspectiveTagger.assigns,\n",
    "    default_config={}\n",
    ")\n",
    "def rb_perspective_tagger(nlp: Language, name: str):\n",
    "    \"\"\"Spacy component implementation.\n",
    "        Adds the perspective of the text's narrator (\"1st\", \"2nd\" or \"3rd\") to the document.\n",
    "\n",
    "    Args:\n",
    "        nlp: Spacy object.\n",
    "        name: Component name.\n",
    "    \n",
    "    Returns:\n",
    "        `Doc`: A spacy document object.\n",
    "\n",
    "    Returns:\n",
    "        `RbPerspectiveTagger`.\n",
    "    \"\"\"\n",
    "\n",
    "    return RbPerspectiveTagger(nlp)\n",
    "    \n",
    "\n",
    "class RbPerspectiveTagger(PerspectiveTagger):\n",
    "    \"\"\"The class `PerspectiveTagger`.\"\"\"\n",
    "\n",
    "    def __init__(self, nlp: Language):\n",
    "        requires(self, nlp, [\"parser\"])\n",
    "\n",
    "        super().__init__(nlp)\n",
    "    \n",
    "    def __call__(self, doc: Doc) -> Doc:\n",
    "\n",
    "        # #################################\n",
    "        # Change this code and add your own\n",
    "        doc._.perspective = \"3rd\"\n",
    "\n",
    "        return doc"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "0d8a35e7",
   "metadata": {},
   "source": [
    "The `__call__` method of the class does the computation – this is where your code goes. \n",
    "As you can see, the `__call__` method assigns the value `\"3rd\"` to `doc._.perspective`. Although many texts have a 3rd-person narrator, this is probably not a good perspective tagger. You can now improve it by changing the code above.\n",
    "\n",
    "These ideas might help you:\n",
    "- Perspective is often encoded by pronouns (like *ich* 'I', *mich* 'me', *er* 'he', or *ihm* 'him').\n",
    "- The narrator, and therefore also perspective, might change in parts of the text. It is sufficient if only the perspective of the \"main\" narrator (e.g. the most prominent narrator) is tagged.\n",
    "- You can use information provided by other pipeline components, e.g. part-of-speech tags from the Tagger (`token.pos_`) or sentence spans from the Sentencizer (`doc.sents`).\n",
    "\n",
    "If you want to test your implementation, you can continue to execute the following blocks. If you do this repeatedly, do not forget to reload every block again, starting from the block that defines your new pipeline component above.\n",
    "\n",
    "The section **A possible implementation** below also provides an example implementation for a perspective tagger."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "72a77b79",
   "metadata": {},
   "source": [
    "To build your pipeline, you have to **1)** import the pipeline components you need (if you need any) analogously to the Sentencizer below, and **2)** add the components to the pipeline. In the last line below, the perspective tagger is added to the pipeline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "36eb70fa",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<__main__.PerspectiveTagger at 0x7f650a3eafa0>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Create pipeline object\n",
    "nlp = monapipe.model.load()\n",
    "# Add components\n",
    "nlp.add_pipe(\"rb_perspective_tagger\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "61bed5d7",
   "metadata": {},
   "source": [
    "You can print `nlp.pipe_names` to show the components that are part of the pipeline:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "d6bd8c1b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['tok2vec', 'tagger', 'morphologizer', 'trainable_lemmatizer', 'parser', 'ner', 'rb_perspective_tagger']\n"
     ]
    }
   ],
   "source": [
    "print(nlp.pipe_names)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "59a0f814",
   "metadata": {},
   "source": [
    "Test the perspective tagger e.g. on Theodor Fontane's *Der Stechlin* and Gottfried August Bürger's *Münchhausen* (feel free to test on other texts). *Der Stechlin* has a 3rd-person narrator, whereas *Münchhausen* has a 1st-person narrator.\n",
    "The texts are loaded from TextGrid below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "864f1187",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the text from TextGrid repository:\n",
    "text_fontane_stechlin = aggregator.text(\"textgrid:n143.0\").text\n",
    "text_buerger_muenchhausen = aggregator.text(\"textgrid:ktbv.0\").text"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "7efcbb33",
   "metadata": {},
   "source": [
    "Pipe the texts through MONAPipe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "ce67c741",
   "metadata": {},
   "outputs": [],
   "source": [
    "doc_fontane_stechlin = nlp(text_fontane_stechlin)\n",
    "doc_buerger_muenchhausen = nlp(text_buerger_muenchhausen)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "4fec05a7",
   "metadata": {},
   "source": [
    "Get the perspective of each text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "3ebfb741",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3rd\n",
      "3rd\n"
     ]
    }
   ],
   "source": [
    "print(doc_fontane_stechlin._.perspective) # should be `3rd`\n",
    "print(doc_buerger_muenchhausen._.perspective) # should be `1st`"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "04ddab1a",
   "metadata": {},
   "source": [
    "Sometimes it helps to inspect the text or attributes of sentences, clauses or tokens. The following box is meant to be a \"play area\", where you can print whatever you need. Currently, it prints every sentence (among the first 20) that contains any of the tokens *ich* 'I', *mich* 'me', *er* 'he', or *ihm* 'him', together with the part-of-speech tag and the dependency relation of the token."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "c938c3b6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PRON nsubj Gottfried August Bürger\n",
      "Wunderbare Reisen zu Wasser und zu Lande Feldzüge und lustige Abenteuer des Freiherrn von Münchhausen,\n",
      "wie er dieselben bei der Flasche im Zirkel seiner Freunde selbst zu erzählen pflegt. \n",
      "\n",
      "PRON nsubj Reise nach Rußland und St. Petersburg\n",
      "Erstes Kapitel\n",
      "Reise nach Rußland und St. Petersburg\n",
      "Ich trat meine Reise nach Rußland von Haus ab mitten im Winter an, weil ich ganz richtig schloß, daß Frost und Schnee die Wege durch die nördlichen Gegenden von Deutschland, Polen, Kur- und Livland, welche nach der Beschreibung aller Reisenden fast noch elender sind als die Wege nach dem Tempel der Tugend, endlich, ohne besondere Kosten hochpreislicher, wohlfürsorgender Landesregierungen, ausbessern müßte. \n",
      "\n",
      "PRON nsubj Ich reisete zu Pferde, welches, wenn es sonst nur gut um Gaul und Reiter steht, die bequemste Art zu reisen ist. \n",
      "\n",
      "PRON nsubj Ich war nur leicht bekleidet, welches ich ziemlich übel empfand, je weiter ich gegen Nordost hin kam. \n",
      "\n",
      "PRON nsubj \n",
      "Nun kann man sich einbilden, wie bei so strengem Wetter, unter dem rauhesten Himmelsstriche, einem armen, alten Manne zumute sein mußte, der in Polen auf einem öden Anger, über den der Nordost hinschnitt, hilflos und schaudernd dalag und kaum hatte, womit er seine Schamblöße bedecken konnte. \n",
      "\n",
      "PRON nsubj Ob mir gleich selbst das Herz im Leibe fror, so warf ich dennoch meinen Reisemantel über ihn her. \n",
      "\n",
      "PRON obj Plötzlich erscholl eine Stimme vom Himmel, die dieses Liebeswerk ganz ausnehmend herausstrich und mir zurief: »Hol' mich der Teufel, mein Sohn, das soll dir nicht unvergolten bleiben! \n",
      "\n",
      "PRON nsubj «\n",
      "Ich ließ das gut sein und ritt weiter, bis Nacht und Dunkelheit mich überfielen. \n",
      "\n",
      "PRON nsubj Das ganze Land lag unter Schnee; und ich wußte weder Weg noch Steg. \n",
      "\n",
      "PRON nsubj \n",
      "Des Reitens müde, stieg ich endlich ab und band mein Pferd an eine Art von spitzem Baumstaken, der über dem Schnee hervorragte. \n",
      "\n",
      "PRON nsubj Zur Sicherheit nahm ich meine Pistolen unter den Arm, legte mich nicht weit davon in den Schnee nieder und tat ein so gesundes Schläfchen, daß mir die Augen nicht eher wieder aufgingen, als bis es heller lichter Tag war. \n",
      "\n",
      "PRON nsubj Wie groß war aber mein Erstaunen, als ich fand, daß ich mitten in einem Dorf auf dem Kirchhofe lag! \n",
      "\n",
      "PRON nsubj Als ich nun emporsah, so wurde ich gewahr, daß es an den Wetterhahn des Kirchturms gebunden war und von da herunterhing. \n",
      "\n",
      "PRON nsubj Nun wußte ich sogleich, wie ich dran war. \n",
      "\n"
     ]
    }
   ],
   "source": [
    "for sent in list(doc_buerger_muenchhausen.sents)[0:20]:\n",
    "    for token in sent:\n",
    "        if token.text.lower() in [\"ich\", \"mich\", \"er\", \"ihm\"]:\n",
    "            print(token.pos_, token.dep_, sent, \"\\n\")\n",
    "            break"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "c2f518b4",
   "metadata": {},
   "source": [
    "## A possible implementation"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "a60a12ff",
   "metadata": {},
   "source": [
    "A rule-based perspective tagger could look like the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "a7b2d318",
   "metadata": {},
   "outputs": [],
   "source": [
    "# import pipeline components\n",
    "import monapipe.model\n",
    "import monapipe.pipeline.speech_tagger.quotation_marks_speech_tagger\n",
    "from monapipe.pipeline.methods import add_extension, requires\n",
    "\n",
    "# import spaCy classes\n",
    "from spacy.language import Language\n",
    "from spacy.tokens import Doc, Span, Token\n",
    "\n",
    "# import TextGrid components\n",
    "from tgclients.aggregator import Aggregator\n",
    "aggregator = Aggregator()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "7c1efe3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the text from TextGrid repository:\n",
    "text_fontane_stechlin = aggregator.text(\"textgrid:n143.0\").text#[:10000]\n",
    "text_buerger_muenchhausen = aggregator.text(\"textgrid:ktbv.0\").text#[:10000]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "578cf556",
   "metadata": {},
   "outputs": [],
   "source": [
    "@Language.factory(\n",
    "    \"rb_perspective_tagger_solution\",\n",
    "    assigns={\"doc._.perspective\": \"doc._.perspective\"},\n",
    "    default_config={}\n",
    ")\n",
    "def rb_perspective_tagger_solution(nlp: Language, name: str):\n",
    "    \"\"\"Spacy component implementation.\n",
    "        Adds the perspective of the text's narrator (\"1st\", \"2nd\" or \"3rd\") to the document.\n",
    "\n",
    "    Args:\n",
    "        nlp: Spacy object.\n",
    "        name: Component name.\n",
    "    \n",
    "    Returns:\n",
    "        `Doc`: A spacy document object.\n",
    "\n",
    "    Returns:\n",
    "        `PerspectiveTagger`.\n",
    "    \"\"\"\n",
    "\n",
    "    return PerspectiveTaggerSolution(nlp)\n",
    "    \n",
    "\n",
    "class PerspectiveTaggerSolution(PerspectiveTagger):\n",
    "    \"\"\"The class `PerspectiveTaggerSolution`.\"\"\"\n",
    "\n",
    "    def __init__(self, nlp: Language):\n",
    "        requires(self, nlp, [\"parser\"])\n",
    "\n",
    "        # define two new extensions:\n",
    "        add_extension(Doc, \"perspective\") # receives the most likely perspective (\"1st\", \"2nd\" or \"3rd\") as string\n",
    "        add_extension(Doc, \"perspective_stats\") # receives probablities for each perspective\n",
    "    \n",
    "    def __call__(self, doc: Doc) -> Doc:\n",
    "\n",
    "        # the for loop iterates over every token in the document and counts\n",
    "        # how often pronouns of each person (\"1st\", \"2nd\" or \"3rd\") occur;\n",
    "        # pronouns within direct speech are ignored\n",
    "        pron_person_counts = {}\n",
    "        for token in doc:\n",
    "            if token.pos_ == \"PRON\" and not (\"direct\" in token._.speech):\n",
    "                if len(token.morph.get(\"Person\")) > 0:\n",
    "                    if token.morph.get(\"Person\")[0] not in pron_person_counts:\n",
    "                        pron_person_counts[token.morph.get(\"Person\")[0]] = 1\n",
    "                    else:\n",
    "                        pron_person_counts[token.morph.get(\"Person\")[0]] += 1\n",
    "        \n",
    "        # the perspective of the document is set to the person of the most pronouns\n",
    "        doc._.perspective = max(pron_person_counts, key=pron_person_counts.get)\n",
    "        \n",
    "        # additionally, the distribution of person in pronouns is saved in the `perspective_stats` attribute;\n",
    "        # for this, the counts are converted to percentages\n",
    "        total_counts = sum(pron_person_counts.values())\n",
    "        doc._.perspective_stats = {person : 1.0*pron_person_counts[person]/total_counts for person in pron_person_counts}\n",
    "        \n",
    "        # return the document\n",
    "        return doc"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "f1db04cd",
   "metadata": {},
   "source": [
    "The perspective tagger from above requires the `.speech` extension from the SpeechTagger, which is loaded and added below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "6eb6a94c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['tok2vec', 'tagger', 'morphologizer', 'trainable_lemmatizer', 'parser', 'quotation_marks_speech_tagger', 'rb_perspective_tagger_solution']\n"
     ]
    }
   ],
   "source": [
    "# build the pipeline\n",
    "nlp = monapipe.model.load(disable=['ner'])\n",
    "nlp.add_pipe(\"quotation_marks_speech_tagger\")\n",
    "nlp.add_pipe(\"rb_perspective_tagger_solution\")\n",
    "print(nlp.pipe_names)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "4c3df41e",
   "metadata": {},
   "source": [
    "Pipe the two test texts:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "dbe2d43b",
   "metadata": {},
   "outputs": [],
   "source": [
    "doc_fontane_stechlin = nlp(text_fontane_stechlin)\n",
    "doc_buerger_muenchhausen = nlp(text_buerger_muenchhausen)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "104848de",
   "metadata": {},
   "source": [
    "And print the values of the new custom attributes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "651ec8da",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3 {'3': 0.9811974270163285, '1': 0.017812963879267688, '2': 0.0009896091044037606}\n",
      "1 {'3': 0.40186915887850466, '1': 0.5970924195223261, '2': 0.0010384215991692627}\n"
     ]
    }
   ],
   "source": [
    "print(doc_fontane_stechlin._.perspective, doc_fontane_stechlin._.perspective_stats)\n",
    "print(doc_buerger_muenchhausen._.perspective, doc_buerger_muenchhausen._.perspective_stats)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env_pipy_public",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
