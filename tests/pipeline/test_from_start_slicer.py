# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
import monapipe.pipeline.slicer.from_start_slicer
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_from_start_slicer():
    """Integration test for the `from_start_slicer`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("from_start_slicer")
    doc = nlp(text_goethe_wv)
    assert check_data_types([doc], "fulltext", str)
