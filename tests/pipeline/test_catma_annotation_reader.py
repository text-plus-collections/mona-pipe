# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import os

import monapipe.model
import monapipe.pipeline.annotation_reader.catma_annotation_reader
from monapipe.annotation import AnnotationList
from tests.methods import check_data_types
from tests.texts import text_goethe_wv, text_goethe_wv_full


def test_catma_annotation_reader():
    """Integration test for the `catma_annotation_reader`."""
    corpus_path = os.path.join(os.path.dirname(__file__), "..", "texts")
    nlp = monapipe.model.load()
    nlp.add_pipe("catma_annotation_reader", config={"corpus_path": corpus_path})
    doc = nlp(text_goethe_wv)
    if text_goethe_wv == text_goethe_wv_full:  # annotations only work for `text_goethe_wv_full`
        assert check_data_types([doc], "annotations", dict, str, AnnotationList)
        assert check_data_types(doc, "annotations", dict, str, AnnotationList)
