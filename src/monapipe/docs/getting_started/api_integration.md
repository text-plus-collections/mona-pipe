# API integration


Several implementations use external APIs instead of running in the installed Python environment. This affects external implementations (such as `dbpedia_spotlight`, `entityfishing`, or `opentapioca`) and, moreover, components of MONAPipe (such as `flair_speech_tagger`, cf. [Table of components and implementations](./component_overview.md#table-of-components-and-implementations)).

For components in MONAPipe, we provide Docker containers to run the API locally. Just start Docker and add the implementation normally by using `add_pipe`, e.g. `nlp.add_pipe("flair_speech_tagger")`:

```
from tgclients.aggregator import Aggregator

import monapipe.model
import monapipe.pipeline.clausizer.dependency_clausizer
import monapipe.pipeline.speech_tagger.flair_speech_tagger

aggregator = Aggregator()

## Create pipeline object
nlp = monapipe.model.load(disable=["ner"])
## Add components
nlp.add_pipe("dependency_clausizer")
nlp.add_pipe("flair_speech_tagger")

print("loading textgrid text...")
text_goethe_wahlverwandtschaften = aggregator.text("textgrid:11hnp.0").text
print("piping doc...")
doc_goethe_wahlverwandtschaften = nlp(text_goethe_wahlverwandtschaften[24399:33069])

for speech_span in doc_goethe_wahlverwandtschaften.spans["speech"]:
    print(speech_span)
    print(speech_span._.speech)
```

We will provide online APIs for affected implementations soon.