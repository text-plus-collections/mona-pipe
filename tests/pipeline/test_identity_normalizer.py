# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import monapipe.model
import monapipe.pipeline.normalizer.identity_normalizer
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_identity_normalizer():
    """Integration test for the `identity_normalizer`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("identity_normalizer")
    doc = nlp(text_goethe_wv)
    assert check_data_types([doc], "text", str)
    assert check_data_types([doc], "text_with_ws", str)
    assert check_data_types(doc, "idx", int)
    assert check_data_types(doc, "text", str)
    assert check_data_types(doc, "text_with_ws", str)
    assert check_data_types(doc, "whitespace_", str)
