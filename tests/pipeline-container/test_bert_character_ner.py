# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import os

import monapipe.model
import monapipe.pipeline.ner.bert_character_ner
from tests.texts import text_goethe_wv


def test_bert_character_ner():
    """Integration test for the `bert_character_ner` component."""
    nlp = monapipe.model.load()
    nlp.add_pipe(
        "bert_character_ner",
        config={"api_mode": ("service" if os.getenv("GITLAB_CI") else "localhost")},
    )
    doc = nlp(text_goethe_wv)
