# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import os

import monapipe.model
import monapipe.pipeline.attribution_tagger.neural_attribution_tagger
import monapipe.pipeline.clausizer.dependency_clausizer
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_neural_attribution_tagger():
    """Integration test for the `neural_attribution_tagger`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("dependency_clausizer")
    nlp.add_pipe(
        "neural_attribution_tagger",
        config={"api_mode": ("service" if os.getenv("GITLAB_CI") else "localhost")},
    )
    doc = nlp(text_goethe_wv)
    assert check_data_types(doc._.clauses, "attribution", set, str)
