<!--readme-about-start-->
# MONAPipe

MONAPipe stands for "Modes of Narration and Attribution Pipeline". It provides natural-language-processing tools for German, implemented in Python/spaCy. In addition to spaCy's default components, MONAPipe adds specific custom components and models for Digital Humanities and Computational Literary Studies.

MONAPipe was originally created in the project group [MONA](https://www.uni-goettingen.de/de/mona/626918.html) and is now further developed within the [Text+](https://text-plus.org/) infrastructure.
<!--readme-about-end-->


## Installation

<!--readme-installation-start-->
Currently, MONAPipe supports Python>=3.9.

Please update to the newest version of pip:

```sh
pip install --upgrade pip
```

Install with pip:

```sh
pip install monapipe
```

To install it in a jupyter notebook run:

```sh
!pip install monapipe
```

For some functionalities, an additional installation of Docker is currently required. Get Docker for your system [here](https://docs.docker.com/get-started/.get-docker/).
<!--readme-installation-end-->


## Usage

Check our [usage section](https://textplus.pages.gwdg.de/collections/mona-pipe/getting_started/getting_started/) to get started.


## Developement

Look into the [development docs](https://textplus.pages.gwdg.de/collections/mona-pipe/development/development/).


## License

Original parts are licensed under LGPL-3.0-or-later. Derivative code and integretated resources are licensed under the respective license of the original (see our section [LICENSES](https://gitlab.gwdg.de/textplus/collections/mona-pipe/-/tree/develop/LICENSES)). Documentation, configuration and generated code files are licensed under CC0-1.0.