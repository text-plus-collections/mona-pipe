# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import os

import monapipe.model
import monapipe.pipeline.clausizer.dependency_clausizer
import monapipe.pipeline.speech_tagger.flair_speech_tagger
from tests.methods import check_data_types
from tests.texts import text_goethe_wv


def test_flair_speech_tagger():
    """Integration test for the `flair_speech_tagger`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("dependency_clausizer")
    nlp.add_pipe(
        "flair_speech_tagger",
        config={"api_mode": ("service" if os.getenv("GITLAB_CI") else "localhost")},
    )
    doc = nlp(text_goethe_wv)
    assert check_data_types(doc.spans["speech"], "speech", dict, str, float)
    assert check_data_types(doc, "speech", dict, str, float)
