# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import os

import monapipe.model
import monapipe.pipeline.clausizer.dependency_clausizer
import monapipe.pipeline.gen_tagger.neural_gen_tagger
from tests.methods import check_data_types
from tests.texts import text_goethe_wv, text_goethe_wv_full


def test_neural_gen_tagger():
    """Integration test for the `neural_gen_tagger`."""
    nlp = monapipe.model.load()
    nlp.add_pipe("dependency_clausizer")
    nlp.add_pipe(
        "neural_gen_tagger",
        config={"api_mode": ("service" if os.getenv("GITLAB_CI") else "localhost")},
    )
    doc = nlp(text_goethe_wv)
    if text_goethe_wv == text_goethe_wv_full:  # gi only occurs in `text_goethe_wv_full`
        assert check_data_types(doc.spans["gi"], "gi", set, str)
